import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'itemName'
})

export class ItemNamePipe implements PipeTransform {

    transform(status: string, format: string): string {
        let gender : any = [
            { id: '', status_name: '-'},
            { id: '9', status_name: 'ไม่ระบุ' },
            { id: '1', status_name: 'ชาย' },
            { id: '2', status_name: 'หญิง' }
        ];
        let coc_status: any = [
            { id: '', status_name: '-'},
            { id: '0', status_name: 'ไม่ระบุ' },
            { id: '1', status_name: 'พบผู้ป่วย' },
            { id: '2', status_name: 'ไม่พบผู้ป่วย' },
            { id: '3', status_name: 'ผู้ป่วยเสียชีวิต' }
        ];
        let med_use: any = [
            { id: '', status_name: '-'},
            { id: '0', status_name: 'ไม่มีปัญหา' },
            { id: '1', status_name: 'มีปัญหา' },
        ];
        let self_use_list: any = [
            { id: '', status_name: '-'},
            { id: '0', status_name: 'ไม่มี' },
            { id: '1', status_name: 'กัญชา' },
            { id: '9', status_name: 'มีใช้ยาอื่นๆที่หาซื้อเอง' },
        ];
        let drugs_use: any = [
            { id: '', status_name: '-'},
            { id: '0', status_name: 'ไม่มี' },
            { id: '1', status_name: 'บุหรี่' },
            { id: '2', status_name: 'สุรา' },
            { id: '9', status_name: 'อื่นๆ' }
        ];
        let disablity: any = [
            { id: '', status_name: '-'},
            { id: '0', status_name: 'ไม่มี' },
            { id: '1', status_name: 'มี' },
            { id: '9', status_name: 'ไม่เข้าเกณฑ์พิการ' },
        ];     
        let result: any = [
            { id: '', status_name: '-'},
            { id: '1', status_name: 'ดีขึ้น' },
            { id: '2', status_name: 'คงที่' },
            { id: '3', status_name: 'แย่ลง' },
            { id: '9', status_name: 'ประเมินไม่ได้' },
        ];      
        let plan: any = [
            { id: '', status_name: '-'},
            { id: '9', status_name: 'ไม่จำเป็นต้องติดตามต่อ' },
            { id: '1', status_name: 'ส่งต่อโรงพยาบาลชุมชน' },
            { id: '2', status_name: 'ส่งต่อโรงพยาบาลทั่วไป' },
            { id: '3', status_name: 'ส่งต่อโรงพยาบาลโรงพยาบาลศูนย์' },
            { id: '4', status_name: 'ส่งต่อโรงพยาบาลโรงพยาบาลนอกสังกัด' },
            { id: '8', status_name: 'ติดตามเยี่ยมต่อไป' },
        ];
        let text_result : any;      
        if(format == 'coc_status'){
            text_result = coc_status.find(x => x.id === status).status_name;
        }else if(format == 'med_use'){
            text_result = med_use.find(x => x.id === status).status_name;
        }else if(format == 'self_use'){
            text_result = self_use_list.find(x => x.id === status).status_name;
        }else if(format == 'drugs_use'){
            text_result = drugs_use.find(x => x.id === status).status_name;
        }else if(format == 'disablity'){
            text_result = disablity.find(x => x.id === status).status_name;
        }else if(format == 'result'){
            text_result = result.find(x => x.id === status).status_name;
        }else if(format == 'plan'){
            text_result = plan.find(x => x.id === status).status_name;
        } else if(format == 'gender'){
            text_result = gender.find(x => x.id === status).status_name;
        } else {
            text_result =  '-';
        }
        return text_result;
    }
}

