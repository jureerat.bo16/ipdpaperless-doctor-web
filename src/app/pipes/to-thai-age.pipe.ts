
import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
    name: 'thaiage'
})
export class ThaiAgePipe implements PipeTransform {
  transform(age: string): string { 
    // console.log(age);
    if (!age) {
      return 'ไม่สามารถแปลงได้'; // Return an empty string if age is undefined or null
    }
    let _age:string ;
    _age = age;
    let myArray = _age.split("-");
    // console.log(myArray);
    let tAge = parseInt(myArray[0])+" ปี "+parseInt(myArray[1])+" เดือน "+parseInt(myArray[2])+" วัน ";
   
    return tAge;
  }
}