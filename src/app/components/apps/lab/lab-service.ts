import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LabService {

  pathPrefixLookup: any = `api-lookup/lookup`
  pathPrefixDoctor: any = `api-doctor/doctor`
  pathPrefixAuth: any = `api-auth/auth`
  pathPreficNure:any = `api-nurse/nurse`

  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixDoctor}`
  })

  private axiosLookup = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
  })
  private axiosLab = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPreficNure}`
  })

  constructor() 
  {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });
    this.axiosLab.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })

    this.axiosLookup.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosLookup.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })
  }
  // e7d537ce-244d-429c-ad95-6d492e4ef5e6
  async getListlaborder(admitId:any) {
    const url = `/doctor-order/${admitId}/admit`
    return await this.axiosInstance.get(url)
  }
  async getItemlab(id:any) {
    const url = `/items/getByID/${id}`;
    return await this.axiosLookup.get(url)
  }
  async getListlabbycode(id:any,code:any) {
    
    // console.log("ans :",ans);
    // id = "66000124";
    const url = `/his-services/waiting-lab?an=${id}&code=${code}`

    console.log('api ekg:',url);
    // console.log(this.axiosEkg.get(url));
    return await this.axiosLab.get(url)
  }
  async getPatientInfo(admitId: any) {
    // console.log(admitId);

    const url = `/admit/${admitId}/patient-info`
    // console.log(url);

    return await this.axiosInstance.get(url)
  }

}
