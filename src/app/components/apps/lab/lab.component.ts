import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { LabService } from './lab-service';
import { MenuItem, MessageService } from 'primeng/api';

@Component({
    selector: 'app-lab',
    templateUrl: './lab.component.html',
    styleUrls: ['./lab.component.scss'],
    providers: [MessageService],
})
export class LabComponent implements OnInit {
    queryParamsData: any;
    patientList:any = [];
    patientLists:any ;
    labtList: any;
    an: any;
    hn: any;
    title: any;
    fname: any;
    lname: any;
    gender: any;
    age: any;
    phone: any;
    list: any = [];
    items: any[] = [];
    listlab: any = [];
    activeIndex: number = 0;
    speedDialitems: MenuItem[] = [];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private spinner: NgxSpinnerService,
        // private renderer: Renderer2,
        // private messageService:MessageService,
        private labService: LabService,
        private messageService: MessageService
    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;
        console.log(this.queryParamsData);
    }
     ngOnInit() {
        this.getPatient();
        this.buttonSpeedDial();
    }
    // changeItem(i: number) {
    //     this.activeIndex = i;
        
    // }
    changeItem(i: number) {
        this.messageService.clear();
        if (this.listlab[i].questions[0]?.header) { // ตรวจสอบว่าข้อมูลใน listlab ที่ index i มี property label หรือไม่
            this.activeIndex = i;
        } else{
            this.activeIndex = i;
            this.messageService.add({
                severity: 'warn',
                summary: 'ไม่มีผลแลบ',
                detail: 'ผลแลบยังไม่ออกครับ',
            });
        }
    }
    
    //ข้อมูลคนป่วย
    async getPatient() {
        // console.log('getPatient');
        try {
            const response = await this.labService.getPatientInfo(
                this.queryParamsData
            );
            console.log(response);
            
            // const data: any = response.data;

            this.patientList =   response.data.data;
            this.patientLists =   response.data.pateint;
            console.log(this.patientList);

            this.hn = this.patientList.hn;
            this.an = this.patientList.an;
            this.title = this.patientList.patient.title;
            this.fname = this.patientList.patient.fname;
            this.lname = this.patientList.patient.lname;
            this.gender = this.patientList.patient.gender;
            this.age = this.patientList.patient.age;
            this.phone = this.patientList.phone;

            console.log('an : ', this.an);
            if (this.an != null || this.an != undefined) {
                this.getListlabdata();
            }
        } catch (error: any) {
            console.log(error);
        }
    }
    async getListlabdata() {
        // this.spinner.show();
        try {
            // console.log("this.an : ",this.an);
            const response = await this.labService.getListlaborder(this.queryParamsData);
            console.log('responsedsfsdf  : ', response);

            const data = await response.data.data;
            this.labtList = response.data.data;
            console.log(this.labtList);
            for (let items of data) {
                if (items.order.length > 1  ) {

                    for (let r of items.order) {
                      //list lab old
                      if(r.item_type_id == 1){

                        let item = {
                          labmame: r.item_name,
                          id: r.item_id,
                          order_date: new Date(
                              items.doctor_order_date
                          ).toLocaleString('th-TH'),
                          order_time: items.doctor_order_time,
                          order_id: r.id,
                      };
                      this.list.push(item);

                      }

                    }
                }
            }
            for (let l of this.list) {
                let listdata = {
                    id: l.id,
                    label: l.labmame,
                    icon: 'fa-solid fa-vial-virus',
                    labcode: '',
                    questions: [],
                };
                if (this.listlab.find((i: any) => i.id === l.id)) {
                } else {
                    try {
                        let i: any = await this.labService.getItemlab(l.id);
                        listdata.labcode = i.data.data[0].code;
                        let results: any =
                            await this.labService.getListlabbycode(
                                this.an,
                                listdata.labcode
                            );
                        console.log('reseults:', results.data.length);
                        let date = '';
                        let time = '';
                        let labresult: any = [];
                        let lab: any = [];
                        let lab_date: any = [];
                        if (results.data.length > 0) {
                            for (i = 0; i < results.data.length; i++) {
                                console.log('i:', i);
                                lab_date =
                                    results.data[i].lab_test_result_date+ ' ' +results.data[i].lab_test_result_time
                                
                                if (
                                    results.data[i].lab_test_result_date !=
                                        date ||
                                    results.data[i].lab_test_result_time != time
                                ) {
                                    let data = results.data.filter(
                                        (x: any) =>
                                            x.lab_test_result_date == date ||
                                            x.lab_test_result_time != time
                                    );
                                    let result = {
                                        header: lab_date,
                                        result: data,
                                    };
                                    console.log(result);
                                    lab.push(result);
                                }
                                date = results.data[i].lab_test_result_date;
                                time = results.data[i].lab_test_result_time;
                            }
                        }

                        listdata.questions = lab;
                        
                    } catch (e) {}
                    this.listlab.push(listdata);
                }
            }
            console.log('items items ', this.items);
            console.log('listlab listlab ', this.listlab);

            //   for (let item of data) {
            //     // ดึงค่า image_url จากแต่ละอ็อบเจ็กต์และเพิ่มเข้าใน images
            //     this.images.push(item.image_url);
            // }
            // console.log(this.images);

            //   this.spinner.hide(); // แก้ไข this.hideSpinner() เป็น this.spinner.hide()
            //   console.log(this.images.length);
            //   if (this.images.length === 0) {
            //     this.isCardVisible = false;
            //     this.messageService.add({
            //         severity: 'warn',
            //         summary: 'ไม่มีรูปภาพ EKG',
            //         detail: 'ภาพ EKG ไม่มีในระบบไม่สามารถดูรูปภาพครับ',
            //     });

            // }
        } catch (error: any) {
            // console.log(error);
            this.spinner.hide(); // แก้ไข this.hideSpinner() เป็น this.spinner.hide()
        }
    }
    backlistPatient() {
        this.router.navigate(['/list-patient']);
    }

    buttonSpeedDial(){
        this.speedDialitems = [
          {
            icon: 'pi pi-arrow-left',
            routerLink: ['/list-patient'],
            tooltipOptions: {
              tooltipLabel: 'ย้อนกลับ',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-user-doctor',
            command: () => {
              this.navigateDoctorOrder()
            },
            tooltipOptions: {
              tooltipLabel: 'Doctor Order',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-circle-info',
            command: () => {
              this.navigatePatientInfo()
            },
            tooltipOptions: {
              tooltipLabel: 'Patient Info',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-comment-medical',
            command: () => {
              this.navigateAdmisstionNote()
            },
            tooltipOptions: {
              tooltipLabel: 'Admission Note',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-heart-pulse',
            command: () => {
              this.navigateEkg()
            },
            tooltipOptions: {
              tooltipLabel: 'EKG',
              tooltipPosition: 'bottom'
            },
          },
          {
            icon: 'fa-solid fa-phone-volume',
            command: () => {
              this.navigateConsult()
            },
            tooltipOptions: {
              tooltipLabel: 'Consult',
              tooltipPosition: 'bottom'
            },
          },
        ];
      }  

    navigatePatientInfo(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/patient-info'], { queryParams: { data: jsonString } });

    }

    navigateDoctorOrder(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log('admit id:',jsonString);   
    this.router.navigate(['/doctor-order'], { queryParams: { data: jsonString } });

    }

    navigateAdmisstionNote(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/admission-note'], { queryParams: { data: jsonString } });

    }

    navigateConsult(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/consult'], { queryParams: { data: jsonString } });

    }

    navigateEkg(){
        let jsonString = JSON.stringify(this.queryParamsData);
        console.log(jsonString);   
        this.router.navigate(['/ekg'], { queryParams: { data: jsonString } });
    
      }

}
