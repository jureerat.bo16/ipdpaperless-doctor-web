import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MenuItem, MessageService } from 'primeng/api';
import { PatientInfoService } from './patient-info-service';

@Component({
  selector: 'app-patient-info',
  templateUrl: './patient-info.component.html',
  styleUrls: ['./patient-info.component.scss'],
  providers: [MessageService],
})
export class PatientInfoComponent implements OnInit{

  // items: any;
  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  admit_date: any;
  hn: any;
  an: any;
  title: any;
  fname: any;
  lname: any;
  gender: any;
  age: any;
  address: any;
  phone: any;

  chief_complaint: any;
  present_illness: any;
  past_history: any;
  body_temperature: any;
  systolic_blood_pressure: any;
  diatolic_blood_pressure: any;
  pulse_rate: any;
  respiratory_rate: any;
  body_weight: any;
  body_height: any;
  physical_exam: any;
  pre_diag: any;
  doctor_fname: any;
  doctor_lname: any;
  admit_time: any;

  queryParamsData: any;
  patientList: any;

  cid: any;
  nationality: any;
  religion: any;
  blood_group: any;
  inscl_name: any;
  occupation: any;
  reference_person_phone: any;
  reference_person_name: any;
  reference_person_relationship: any;

  speedDialitems: MenuItem[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private messageService: MessageService,
    private patientInfoService: PatientInfoService,
) {
    let jsonString: any =
        this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
}

  ngOnInit(): void {
    this.getPatient();
    this.buttonSpeedDial();
  }

  async getPatient() {
    try {

        const _limit = this.pageSize;
        const _offset = this.offset;

        const response = await this.patientInfoService.getPatientInfo(
            this.queryParamsData
        );

        const data: any = response.data;

        this.patientList = await data.data;
        console.log(this.patientList);
        this.admit_date = this.patientList.admit_date;
        this.hn = this.patientList.hn;
        this.an = this.patientList.an;
        this.title = this.patientList.patient.title;
        this.fname = this.patientList.patient.fname;
        this.lname = this.patientList.patient.lname;
        this.gender = this.patientList.patient.gender;
        this.age = this.patientList.patient.age;
        this.phone = this.patientList.phone;

        this.cid = this.patientList.patient.cid;
        this.nationality = this.patientList.patient.nationality;
        this.religion = this.patientList.patient.religion;
        this.blood_group = this.patientList.patient.blood_group;
        this.inscl_name = this.patientList.patient.inscl_name;
        this.address = this.patientList.patient.address;
        this.occupation = this.patientList.patient.occupation;
        this.reference_person_phone = this.patientList.patient.reference_person_phone;
        this.reference_person_name = this.patientList.patient.reference_person_name;
        this.reference_person_relationship = this.patientList.patient.reference_person_relationship;

        this.chief_complaint = this.patientList.opd_review.chief_complaint;
        this.present_illness = this.patientList.opd_review.present_illness;
        this.past_history = this.patientList.opd_review.past_history;
        this.body_temperature = this.patientList.opd_review.body_temperature;
        this.systolic_blood_pressure = this.patientList.opd_review.systolic_blood_pressure;
        this.diatolic_blood_pressure = this.patientList.opd_review.diatolic_blood_pressure;
        this.pulse_rate = this.patientList.opd_review.pulse_rate;
        this.respiratory_rate = this.patientList.opd_review.respiratory_rate;
        this.body_weight = this.patientList.opd_review.body_weight;
        this.body_height = this.patientList.opd_review.body_height;
        this.physical_exam = this.patientList.opd_review.physical_exam;
        this.pre_diag = this.patientList.pre_diag;
        this.doctor_fname = this.patientList.doctor.fname;
        this.doctor_lname = this.patientList.doctor.lname;

        this.admit_time = this.patientList.admit_time
        this.total = data.total || 1;
        this.messageService.add({
            severity: 'success',
            summary: 'กระบวนการสำเร็จ #',
            detail: '.....',
        });
    } catch (error: any) {
        console.log(error);
        // this.message.error('getPatient()' +`${error.code} - ${error.message}`);
        this.messageService.add({
            severity: 'dager',
            summary: 'เกิดข้อผิดพลาด #',
            detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
        });
    }
  }

  back() {
    this.router.navigate(['/list-patient']);
  }

  navigateDoctorOrder(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log('admit id:',jsonString);   
    this.router.navigate(['/doctor-order'], { queryParams: { data: jsonString } });

  }

  navigateAdmisstionNote(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/admission-note'], { queryParams: { data: jsonString } });

  }

  navigateEkg(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/ekg'], { queryParams: { data: jsonString } });

  }

  navigateConsult(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/consult'], { queryParams: { data: jsonString } });

  }
  navigateLab(){
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);   
    this.router.navigate(['/lab'], { queryParams: { data: jsonString } });

  }

  buttonSpeedDial(){
    this.speedDialitems = [
      {
        icon: 'pi pi-arrow-left',
        routerLink: ['/list-patient'],
        tooltipOptions: {
          tooltipLabel: 'ย้อนกลับ',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-user-doctor',
        command: () => {
          this.navigateDoctorOrder()
        },
        tooltipOptions: {
          tooltipLabel: 'Doctor Order',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-comment-medical',
        command: () => {
          this.navigateAdmisstionNote()
        },
        tooltipOptions: {
          tooltipLabel: 'Admission Note',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-heart-pulse',
        command: () => {
          this.navigateEkg()
        },
        tooltipOptions: {
          tooltipLabel: 'EKG',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-phone-volume',
        command: () => {
          this.navigateConsult()
        },
        tooltipOptions: {
          tooltipLabel: 'Consult',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-vial-virus',
        command: () => {
          this.navigateLab()
        },
        tooltipOptions: {
          tooltipLabel: 'Lab',
          tooltipPosition: 'bottom'
        },
      }
    ];
  }

}
