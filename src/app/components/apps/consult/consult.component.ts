import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MenuItem, MessageService } from 'primeng/api';
import { ConsultInfoService } from './consult-info-service';
import { DateTime } from 'luxon';
import * as _ from 'lodash';
import { log } from 'console';





@Component({
  selector: 'app-consult',
  templateUrl: './consult.component.html',
  styleUrls: ['./consult.component.scss'],
  providers: [MessageService],
})
export class ConsultComponent implements OnInit {

  hn: any;
  an: any;
  title: any;
  fname: any;
  lname: any;
  gender: any;
  age: any;
  address: any;
  phone: any;
  queryParamsData: any;
  patientList: any;

  progressValueS: any;
  progressValueO: any;
  progressValueA: any;
  progressValueP: any;
  progressValueN: any;
  progress_note_subjective: any[] = [];
  progress_note_objective: any[] = [];
  progress_note_assertment: any[] = [];
  progress_note_plan: any[] = [];
  progress_note: any[] = [];
  // doctor order id
  doctorOrderData: any;
  is_new_order: boolean = true;
  isNewOrder: boolean = true;
  is_success: boolean = false;

  // user
  userData: any;

  consultValue: any;
  consult_note: any = [];
  consult_notevalue : any=[];

  //doctor id
  itemDoctor: any = [];
  Doctor_userid: any = '';
  fullnamedoctor: any = '';

  speedDialitems: MenuItem[] = [];
  doctorOrder: any;

  constructor(
    private router: Router,
    private consultInfoService: ConsultInfoService,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private messageService: MessageService,

  ) {

    let jsonString: any =
      this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;

    this.getDoctorOrder();

    let _user = sessionStorage.getItem('userData');
    if (_user != 'undefined' && _user != null) {
      let user = JSON.parse(_user!);
      this.userData = user;
    } else {
      this.userData = null;
    }
  }

  exampleData: any;

  ngOnInit(): void {


    this.getLookupDoctor();
    this.getPatient();
    this.getolistconsult_note();
    this.buttonSpeedDial();

  }

  async getLookupDoctor() {
    let lDoctors: any = await this.consultInfoService.getLookupDoctor();
    this.itemDoctor = lDoctors.data;
    // console.log('item:', this.itemDoctor);
  }

  async getLookupDoctorinfoByID(id: any) {

    if (id == '' || id == null) {
      return this.fullnamedoctor = '';
    } else {
      if (Array.isArray(this.itemDoctor) && this.itemDoctor.length > 0) {
        let username = this.itemDoctor.find((x: any) => x.id == id);
        // console.log('name_doctor:', username);
        if (username) {
          // return username.data.data[0].title + username.data.data[0].fname + ' ' + username.data.data[0].lname;
          return username.title + username.fname + ' ' + username.lname;
        } else {
          return '';
        }
      } else {
        return '';
      }

    }

  }
  async getDoctorOrder() {
    let admitId = this.queryParamsData;
    let datas = await this.consultInfoService.getDoctorOrderById(admitId);
    let sortData = _.orderBy(datas.data.data, ["doctor_order_date", "doctor_order_time"], ['desc', 'desc']);
    this.doctorOrder = sortData;
    this.isNewOrder = sortData[0].is_confirm;
    this.is_new_order = sortData[0].is_confirm;
    var date2 = DateTime.now();
    var date3 = DateTime.now();
    const date5 = date2.toFormat('yyyy-MM-dd');
    const time1 = date3.toFormat('HH:mm:ss');
    let doctor_order = [{
      "id": sortData[0].id,
      "admit_id": sortData[0].admit_id,
      "doctor_order_date": sortData[0].doctor_order_date,
      "doctor_order_time": sortData[0].doctor_order_time,
      "doctor_order_by": sortData[0].doctor_order_by,
      "is_confirm": sortData[0].is_confirm,
      "is_active": true,
      "create_date": sortData[0].doctor_order_date,
      "create_by": null,
      "modify_date": date5,
      "modify_by": this.userData.id || null,
      "status": "pending",
      "confirm_by": null,
      "confirm_date": null,
      "confirm_time": null
    }]
    this.doctorOrderData = doctor_order;
   //  console.log(this.doctorOrder);
  }

  async getPatient() {
    try {
      // console.log('xxx', this.queryParamsData);
      const response = await this.consultInfoService.getPatientInfo(this.queryParamsData);

      const data: any = response.data;

      this.patientList = await data.data;
      // console.log('consultxx', this.patientList);
      this.hn = this.patientList.hn;
      this.an = this.patientList.an;
      this.title = this.patientList.patient.title;
      this.fname = this.patientList.patient.fname;
      this.lname = this.patientList.patient.lname;
      this.gender = this.patientList.patient.gender;
      this.age = this.patientList.patient.age;
      this.address = this.patientList.address;
      this.phone = this.patientList.phone;
      this.messageService.add({
        severity: 'success',
        summary: 'กระบวนการสำเร็จ #',
        detail: '.....',
      });
    } catch (error: any) {
      console.log(error);
      // this.message.error('getPatient()' +`${error.code} - ${error.message}`);
      this.messageService.add({
        severity: 'dager',
        summary: 'เกิดข้อผิดพลาด #',
        detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
      });
    }
  }

  backlistPatient() {
    sessionStorage.removeItem('doctor_order');
    this.router.navigate(['/list-patient']);
  }


  async addProgressNote() {
    await this.checkOrder(); // check if doctor order is exist
    // console.log(this.is_success);

    let progress_note: any; // set progress note
    if (this.is_success) {

      // this.consult_note.push({ label: this.consultValue });
      progress_note = {
        note: [this.consultValue],
      };
      // this.progressValueN = '';
      //console.log("fonk", progress_note);

      //console.log('user_id', this.Doctor_userid);
      await this.saveProgressNote(progress_note); // save progress note
      // await this.getolistconsult_note();

    } else {
      alert('ไม่สามารถบันทึกข้อมูลได้');
    }
    this.getolistconsult_note();
  }

  async saveProgressNote(datas: any) {
    console.log(datas);
    // set data to save
    if (this.consultValue == '') {
      alert('กรุณาระบุรายละเอียดการ consult');

    } else {
      let data = {
        doctor_order: this.doctorOrderData,
        progress_note: [{ 'note': this.consultValue }],
      };
      //console.log('save consult:', data);

      try {
        const response = await this.consultInfoService.saveDoctorOrder(data);
        //console.log(response);
        this.consultValue = '';
        await this.getolistconsult_note();
      } catch (error) {
        console.log('saveDoctorOrder:' + error);
        alert('บันทึกไม่สำเร็จ');

      }

    }

  }

  async checkOrder() {
    if (this.is_new_order) {
      var date2 = DateTime.now();
      var date3 = DateTime.now();

      const date5 = date2.toFormat('yyyy-MM-dd');
      const time1 = date3.toFormat('HH:mm:ss');

      let data = {
        doctor_order: [{
          admit_id: this.queryParamsData,
          doctor_order_date: date5,
          doctor_order_time: time1,
          doctor_order_by: this.userData.id || null,
          is_confirm: false,
        }],
      };
      //console.log('send data:', data);
      try {
        let rs = await this.consultInfoService.saveDoctorOrder(data);
        //console.log('response:', rs);
        this.doctorOrderData = rs.data.data.doctor_order;
        this.is_new_order = false;
        sessionStorage.setItem(
          'doctor_order',
          JSON.stringify(this.doctorOrderData)
        );
        //console.log('doctorOrderData:', this.doctorOrderData);
        this.is_success = true;
      } catch (error) {
        console.log('save Doctor Order:' + error);
        this.is_success = false;
      }
    } else {
      this.is_success = true;
    }

  }


  async getolistconsult_note() {
    console.log('pa:'+this.queryParamsData)
    let docterOrder = await this.consultInfoService.getDoctorOrderById(this.queryParamsData);
    let listDoctorOrder = docterOrder.data.data;
    console.log('xxx', listDoctorOrder);
    this.consult_note = [];
    // this.consult_notevalue = [];

    // if (listDoctorOrder.is_confirm){
      
      for (let i of listDoctorOrder) {
        this.consultValue = '';
        if(!i.is_confirm){
          console.log('False');         
          let note: any = i.progress_note;
          let user: any = await this.getLookupDoctorinfoByID(i.doctor_order_by);
          // console.log('yyy'+user);
    
          if (note && note.note != null) {
            let data = {
              id: i.id,
              date: i.doctor_order_date,
              time: i.doctor_order_time,
              note: i.progress_note.note || '',
              user: user
    
            }
            this.consult_note.push(data);

            this.consultValue += i.progress_note.note
  
            console.log(this.consultValue);

          }
        }else {
          console.log('True');
          this.consultValue = '';
          let note: any = i.progress_note;
          let user: any = await this.getLookupDoctorinfoByID(i.doctor_order_by);
          // console.log(user);
    
          if (note && note.note != null) {
    
    
            let data = {
              id: i.id,
              date: i.doctor_order_date,
              time: i.doctor_order_time,
              note: i.progress_note.note || '',
              user: user
    
            }
            this.consult_note.push(data);
            // console.log(data);
          }

        }

      }     

    }

    // กดปุ่ม Send แล้วให้แสดงทุกกรณี
    // for (let i of listDoctorOrder) {
    //   let note: any = i.progress_note;
    //   let user: any = await this.getLookupDoctorinfoByID(i.doctor_order_by);
    //   // console.log(user);

    //   if (note && note.note != null) {


    //     let data = {
    //       id: i.id,
    //       date: i.doctor_order_date,
    //       time: i.doctor_order_time,
    //       note: i.progress_note.note || '',
    //       user: user

    //     }
    //     this.consult_note.push(data);
    //     // console.log(data);
    //   }
    // }




  navigateDoctorOrder() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log('admit id:', jsonString);
    this.router.navigate(['/doctor-order'], { queryParams: { data: jsonString } });

  }

  navigateAdmisstionNote() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log(jsonString);
    this.router.navigate(['/admission-note'], { queryParams: { data: jsonString } });

  }

  navigateEkg() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log(jsonString);
    this.router.navigate(['/ekg'], { queryParams: { data: jsonString } });

  }

  navigatePatientInfo() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log(jsonString);
    this.router.navigate(['/patient-info'], { queryParams: { data: jsonString } });

  }

  navigateLab() {
    let jsonString = JSON.stringify(this.queryParamsData);
    // console.log(jsonString);
    this.router.navigate(['/lab'], { queryParams: { data: jsonString } });

  }

  buttonSpeedDial() {
    this.speedDialitems = [
      {
        icon: 'pi pi-arrow-left',
        routerLink: ['/list-patient'],
        tooltipOptions: {
          tooltipLabel: 'ย้อนกลับ',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-user-doctor',
        command: () => {
          this.navigateDoctorOrder()
        },
        tooltipOptions: {
          tooltipLabel: 'Doctor Order',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-circle-info',
        command: () => {
          this.navigatePatientInfo()
        },
        tooltipOptions: {
          tooltipLabel: 'Patient Info',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-comment-medical',
        command: () => {
          this.navigateAdmisstionNote()
        },
        tooltipOptions: {
          tooltipLabel: 'Admission Note',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-heart-pulse',
        command: () => {
          this.navigateEkg()
        },
        tooltipOptions: {
          tooltipLabel: 'EKG',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-vial-virus',
        command: () => {
          this.navigateLab()
        },
        tooltipOptions: {
          tooltipLabel: 'Lab',
          tooltipPosition: 'bottom'
        },
      }
    ];
  }




}


