

import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef } from '@angular/core';
import {
  CanvasWhiteboardComponent,
  CanvasWhiteboardService,
  CanvasWhiteboardUpdate,
  CanvasWhiteboardOptions,
  CanvasWhiteboardShapeService, CircleShape, SmileyShape, StarShape, LineShape, RectangleShape
} from 'ng2-canvas-whiteboard';
import { el } from '@fullcalendar/core/internal-common';
import { AdmisstionNoteService } from '../admission-note-service';
import { Event } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { MinioService } from './minio-service';


@Component({
  selector: 'app-anatomy-drawing',
  templateUrl: './anatomy-drawing.component.html',
  viewProviders: [CanvasWhiteboardComponent],
  styleUrls: ['./anatomy-drawing.component.scss']
})
export class AnatomyDrawingComponent implements OnInit, AfterViewInit {
  @Output() dataEvent = new EventEmitter<string>();
  @ViewChild(CanvasWhiteboardComponent) canvasWhiteboardComponent!: CanvasWhiteboardComponent;


  canvasOptions: CanvasWhiteboardOptions = {};
  private canvas!: HTMLCanvasElement;
  @ViewChild('canvas') canvasRef?: ElementRef<HTMLCanvasElement>;

  private context: CanvasRenderingContext2D | null = null;
  private isDrawing: boolean = false;
  private lastX: number = 0;
  private lastY: number = 0;
  private img = new Image();
  @ViewChild('fileInput') fileInput: any;
  @Input() bufferData: { type: string, data: number[] } | undefined // Assuming you'll receive base64 string

  imageurl: any;
  imageUrls: string | ArrayBuffer | null = '';
  imageUrl: SafeUrl | undefined;

  constructor(
    private canvasWhiteboardService: CanvasWhiteboardService,
    private canvasWhiteboardShapeService: CanvasWhiteboardShapeService,
    private http: HttpClient,
    private cdRef: ChangeDetectorRef,
    private admisstionNoteService: AdmisstionNoteService,
    private sanitizer: DomSanitizer,
    private MinioService: MinioService
  ) {
    this.canvasWhiteboardShapeService.unregisterShapes([CircleShape, SmileyShape, StarShape, LineShape, RectangleShape]);
  }
  ngOnInit(): void {
    // console.log(this.bufferData);
    if (this.bufferData?.type == 'null') {
      this.imageurl = '';

    } else {
      let imgBufferData: any = this.bufferData?.data;
      this.imageurl = String.fromCharCode.apply(String, imgBufferData);
    }


    // console.log(this.imageurl);
    this.getImageFromMinio();

  }
  ngAfterViewInit(): void {

    this.cdRef.detectChanges(); // Trigger change detection
    this.canvas = this.canvasWhiteboardComponent.canvas.nativeElement;

    // Load your image

    this.img.onload = () => {
      // Draw the image onto the canvas if context is not null
      const context = this.canvas.getContext('2d');
      if (context) {
        context.drawImage(this.img, 0, 0, this.canvas.width, this.canvas.height);
      } else {
        console.error('Canvas context is null.');
      }
    };
    if (this.imageurl == '') {
     // this.img.src = 'http://209.15.114.75:40019/uploads/1716833201422'; // Replace with your background image path
      this.img.src = '../../../../assets/images/body.jpg'; // Replace with your background image path

    } else {

      this.img.src = this.imageurl; // Replace with your background image path
    }



  }
  async getImageFromMinio(){
    const bucketName = 'uploads';
    const objectKey = '1716831302377';
    const signedUrl = this.MinioService.getSignedUrl(bucketName, objectKey);

    try {
      const blob = await this.MinioService.getImage(signedUrl);
      const objectURL = URL.createObjectURL(blob);
      this.imageUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    } catch (error) {
      console.error('Error fetching image from Minio:', error);
    }
  }
  // async getImageFromMinio(){
  //   const bucketName = 'uploads';
  //   const objectKey = '1716831302377';
  //   try {
  //   const signedUrl = this.MinioService.getFile(bucketName, objectKey);
  //   console.log(signedUrl);
      
  //   } catch (error) {
  //     console.error('Error fetching image from Minio:', error);
      
  //   }

    // try {
    //   const blob = await this.MinioService.getImage(signedUrl);
    //   const objectURL = URL.createObjectURL(blob);
    //   this.imageUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    // } catch (error) {
    //   console.error('Error fetching image from Minio:', error);
    // }
  // }



  onCanvasClear() {
    console.log('onCanvasClear');

    // Draw the image onto the canvas if context is not null
    const context = this.canvas.getContext('2d');
    if (context) {
      context.drawImage(this.img, 0, 0, this.canvas.width, this.canvas.height);
    } else {
      console.error('Canvas context is null.');
    }

    this.img.src = '../../../../assets/images/body.jpg'; // Replace with your background image path

    const canvas = this.canvasRef?.nativeElement;
    if (canvas) {
      this.context = canvas.getContext('2d');
      this.setupEventListeners(canvas);
    }

  }
  onCanvasUndo(e: Event) {
    this.img.src = '../../../../assets/images/body.jpg'; // Replace with your background image path

    const canvas = this.canvasRef?.nativeElement;
    if (canvas) {
      this.context = canvas.getContext('2d');
      this.setupEventListeners(canvas);
    }
  }


  saveToStorage(): void {
    // Get the current full update history
    const updates: Array<CanvasWhiteboardUpdate> = this.canvasWhiteboardComponent.getDrawingHistory();
    // Stringify the updates, which will return an Array<string>
    const stringifiedUpdatesArray: Array<string> = updates.map(update => update.stringify());
    // Stringify the Array<string> to get a "string", so we are now ready to put this item in the storage
    const stringifiedStorageUpdates: string = JSON.stringify(stringifiedUpdatesArray);
    // Save the item in storage of choice
    sessionStorage.setItem('canvasWhiteboardDrawings', stringifiedStorageUpdates);
  }

  loadFromStorage(): void {
    // Get the "string" from the storage
    const canvasDrawingsJson: any = sessionStorage.getItem('canvasWhiteboardDrawings');
    // Null check
    if (canvasDrawingsJson != null) {
      // Parse the string, and get an Array<string>
      const parsedStorageUpdates: Array<string> = JSON.parse(canvasDrawingsJson);
      // Parse each string inside the Array<string>, and get an Array<CanvasWhiteboardUpdate>
      const updates: Array<CanvasWhiteboardUpdate> = parsedStorageUpdates.map(updateJSON =>
        CanvasWhiteboardUpdate.deserializeJson(updateJSON));
      // Draw the updates onto the canvas
      this.canvasWhiteboardService.drawCanvas(updates);
    }
  }



  public changeOptions(): void {
    this.canvasOptions = {
      ...this.canvasOptions,
      fillColorPickerEnabled: false,
      colorPickerEnabled: false
    };
  }
  private setupEventListeners(canvas: HTMLCanvasElement): void {
    canvas.addEventListener('mousedown', this.handleMouseDown.bind(this));
    canvas.addEventListener('mousemove', this.handleMouseMove.bind(this));
    canvas.addEventListener('mouseup', this.handleMouseUp.bind(this));
    canvas.addEventListener('mouseleave', this.handleMouseLeave.bind(this));

    canvas.addEventListener('touchstart', this.handleTouchStart.bind(this));
    canvas.addEventListener('touchmove', this.handleTouchMove.bind(this));
    canvas.addEventListener('touchend', this.handleTouchEnd.bind(this));

  }

  private handleMouseDown(e: MouseEvent): void {
    this.isDrawing = true;
    [this.lastX, this.lastY] = [e.offsetX, e.offsetY];
  }

  private handleMouseMove(e: MouseEvent): void {
    if (!this.context || !this.isDrawing) return;
    this.draw(e.offsetX, e.offsetY);
  }

  private handleMouseUp(): void {
    this.isDrawing = false;
  }

  private handleMouseLeave(): void {
    this.isDrawing = false;
  }

  private handleTouchStart(e: TouchEvent): void {
    const touch = e.touches[0];
    this.isDrawing = true;
    [this.lastX, this.lastY] = [touch.clientX, touch.clientY];
  }

  private handleTouchMove(e: TouchEvent): void {
    if (!this.context || !this.isDrawing) return;
    const touch = e.touches[0];
    this.draw(touch.clientX, touch.clientY);
  }

  private handleTouchEnd(): void {
    this.isDrawing = false;
  }

  private draw(x: number, y: number): void {
    if (!this.context) return;
    this.context.beginPath();
    this.context.moveTo(this.lastX, this.lastY);
    this.context.lineTo(x, y);
    this.context.stroke();
    [this.lastX, this.lastY] = [x, y];
  }

  // Method to handle save button click
  handleSave(event: String | Blob) {
    console.log(event);
    //event.preventDefault(); // Prevent the default download behavior
    setTimeout(() => {
      // const canvas = this.canvasRef?.nativeElement;
      // this.canvas = this.canvasWhiteboardComponent.canvas.nativeElement;
      // if (this.canvas) {
      //   const imageData = this.canvas.toDataURL('image/png');
      //   // Call the uploadImage() method with the base64 image data
      //   console.log(imageData);
      //   this.uploadImage(imageData);
      // } else {
      //   console.error('Canvas is undefined.');
      // }
      console.log('mdfkdjfdjf');
    });
  }

  // sendDataToParent(data: string) {
  //   const canvasWhiteboard = this.canvasWhiteboardComponent.canvas.nativeElement;

  //   // Get the drawing as an image
  //   const imageData = canvasWhiteboard.toDataURL('image/png');
  //   console.log(imageData);
  //   let base64Image = imageData.split(';base64,').pop();
  //   console.log(base64Image);
  //   this.uploadBase64File(imageData);

  //   // this.dataEvent.emit(imageData);
  // }


  ///////////////////////////////////////////////////////////////////////////////////////////////
  // @ViewChild('canvasWhiteboard') canvasWhiteboard: CanvasWhiteboardComponent;


    saveImage() {
    const base64Image = this.canvasWhiteboardComponent.generateCanvasDataUrl('image/png');
    const blob = this.base64ToBlob(base64Image.split(',')[1], 'image/png');
    const fileName = 'drawing.png';

    this.uploadToMinio(blob, fileName).then(response => {
      console.log('Upload successful', response);
    }).catch(error => {
      console.error('Upload failed', error);
    });
  }
    async uploadToMinio(file: Blob, fileName: string): Promise<any> {
    const formData = new FormData();
    formData.append('file', file, fileName);
    this.MinioService.uploadFile(file, fileName).then(response => {
      console.log('Upload successful', response);
    }).catch(error => {
      console.error('Upload failed', error);
    });
    }

  base64ToBlob(base64: string, mimeType: string): Blob {
    const byteCharacters = atob(base64);
    const byteNumbers = new Array(byteCharacters.length);

    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    return new Blob([byteArray], { type: mimeType });
  }
}




  



//1716797056521   http://209.15.114.75:40019/uploads/1716831302377"
// data = {
//   admission_note: [{
//     admit_id: this.queryParamsData,
//     pre_diagnosis: this.diagnosis,
//     plan_of_treatment: this.plan_of_treatment,
//     pictures:imageData
//     physical_examination: {
//       heent: this.heent,
//       heart: this.heart,
//       lung: this.lung,
//       abdomen: this.abdomen,
//       back_and_cva:  this.back_and_cva,
//       extremities_and_skin:  this.extremities_and_skin,
//       neuro_signs:  this.neuro_signs,
//       general_appearance: this.general_appearance,
//       note: this.note,
//     },
//     review_of_system: {       
//       heent: this.pheent,
//       heart: this.pheart,
//       lung: this.plung,
//       abdomen: this.pabdomen,
//       back_and_cva:  this.pback_and_cva,
//       extremities_and_skin:  this.pextremities_and_skin,
//       neuro_signs:  this.pneuro_signs,
//       general_appearance: this.pgeneral_appearance,
//     },
//   }],
//   patient_history: [{
//     admit_id: this.queryParamsData,
//     is_chronic: this.is_chronic,
//     chronic_describe: this.chronic_describe,
//     is_allergy:this.is_allergy,
//     allergy_describe: this.allergy_describe,
//     is_operation:this.is_operation,
//     operation_describe: this.operation_describe,
//     is_family_disease: this.is_family_disease,
//     family_disease_describe: this.family_disease_describe,
//     last_menstrual_period: this.last_menstrual_period,
//     is_child_development_normal: this.is_child_development_normal,
//     is_child_vaccine_complete: this.is_child_vaccine_complete,
//     is_smoke: this.is_smoke,
//     is_drink_alchohol: this.is_drink_alchohol,
//     is_birth_control:this.is_birth_control,
//     smoke_describe: {
//       duration: this.smoke_describe_duration,
//       qty: this.smoke_describe_qty,
//       // frequency: this.smoke_describe_frequency,
//       // frequency: 1,
//     },
//     drink_alchohol_describe: {
//       duration:this.drink_alchohol_describe_duration,
//       qty: this.drink_alchohol_describe_qty,
//       frequency: this.drink_alchohol_describe_frequency,
//     },
//     reference_by: '',
//     modify_date: formattedDate
//   }],
//  };