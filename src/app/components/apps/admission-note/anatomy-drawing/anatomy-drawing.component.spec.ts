import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnatomyDrawingComponent } from './anatomy-drawing.component';

describe('AnatomyDrawingComponent', () => {
  let component: AnatomyDrawingComponent;
  let fixture: ComponentFixture<AnatomyDrawingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AnatomyDrawingComponent]
    });
    fixture = TestBed.createComponent(AnatomyDrawingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
