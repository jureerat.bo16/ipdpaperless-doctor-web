import { Component, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate, Location } from '@angular/common';
import { DateTime } from 'luxon';
import { AdmisstionNoteService } from './admission-note-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Message, MenuItem, MessageService } from 'primeng/api';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';

import * as _ from 'lodash';
import { log } from 'console';

@Component({
  selector: 'app-admission-note',
  templateUrl: './admission-note.component.html',
  styleUrls: ['./admission-note.component.scss'],
  providers: [MessageService],
})

export class AdmissionNoteComponent {
  [x: string]: any;

  ingredient!: string;
  // config page
  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  queryParamsData: any;
  // thaiage: any;
  // pateint info
  admissionNote: any;
  patientInfo: any;
  hn: any;
  an: any;
  title: any;
  fname: any;
  lname: any;
  gender: any;
  age: any;
  age_y: any;
  age_m: any;
  age_d: any;
  address: any;
  phone: any;

  // opd review
  chief_complaint: any;
  present_illness: any;
  past_history: any;
  pre_diag: any;
  // physical exam
  physical_exam: any;
  body_temperature: any;
  body_weight: any;
  body_height: any;
  waist: any;
  pulse_rate: any;
  respiratory_rate: any;
  systolic_blood_pressure: any;
  diatolic_blood_pressure: any;
  oxygen_sat: any;
  eye_score: any;
  movement_score: any;
  verbal_score: any;
  bmi: any;

  // past history
  is_chronic: any;
  chronic_describe: any;
  is_allergy: any;
  allergy_describe: any;
  is_operation: any;
  operation_describe: any;
  is_family_disease: any;
  family_disease_describe: any;
  last_menstrual_period: any;

  // history pregnant
  gravida: any = '0';
  para: any = '0';
  abortion: any = '0';
  live_birth: any = '0';
  gestational_age: any = '';
  is_birth_control: any;
  birth_control_describe: any;

  // history child
  is_child_vaccine_complete: any;

  // drug history
  is_smoke: any;
  smoke_describe: any;
  smoke_describe_duration: any;
  smoke_describe_qty: any;
  smoke_describe_frequency: any;

  is_drink_alchohol: any;
  drink_alchohol_describe: any;
  drink_alchohol_describe_duration: any;
  drink_alchohol_describe_qty: any;
  drink_alchohol_describe_frequency: any;

  is_child_development_normal: any;

  // review of system
  pheent: any;
  pheart: any;
  plung: any;
  pabdomen: any;
  pback_and_cva: any;
  pextremities_and_skin: any;
  pneuro_signs: any;
  pgeneral_appearance: any;

  // physical examination
  heent: any;
  heart: any;
  lung: any;
  abdomen: any;
  back_and_cva: any;
  extremities_and_skin: any;
  neuro_signs: any;
  general_appearance: any;
  note: any;

  // diagnosis && plan of treatment
  diagnosis: string = '';
  plan_of_treatment: string = '';

  modify_date: any;
  messages1: Message[] | undefined;
  messages: any | undefined;


  speedDialitems: MenuItem[] = [];
  doctorOrder: any;
  isNewOrder: boolean = true;
  doctorOrderData: any;
  userData: any;
  
  hb : any ='';
  hct : any ='';
  wbc : any ='';
  plt : any = '';
  pmn : any = '';
  lymph : any = '';
  is_cbc:boolean = false;

  na : any = '';
  k : any = '';
  cl : any = '';
  co2 : any = '';
  is_elyte:boolean = false;

  templates: any =
    {
      "admission_note": [
        {
          "id": "e685eac1-8b39-4b0a-aad8-edd67e60d6d8",
          "admit_id": "ec4adbad-c71d-43ec-9026-3f565a5e807a",
          "pre_diagnosis": "การวินิจฉัยโรค",
          "plan_of_treatment": "แผนการรักษา\n",
          "review_of_system": {
            "lung": false,
            "heart": false,
            "heent": false,
            "abdomen": false,
            "neuro_signs": false,
            "back_and_cva": false,
            "extremities_and_skin": false
          },
          "physical_examination": {
            "heart": false,
            "heent": false,
            "abdomen": false,
            "neuro_signs": false,
            "back_and_cva": false,
            "general_appearance": false,
            "extremities_and_skin": false
          }
        }
      ],
      "patient_history": [
        {
          "admit_id": "ec4adbad-c71d-43ec-9026-3f565a5e807a",
          "is_chronic": true,
          "chronic_describe": null,
          "allergy_describe": "{\"0\"}",
          "is_operation": false,
          "operation_describe": null,
          "is_family_disease": false,
          "family_disease_describe": null,
          "last_menstrual_period": "2024-02-29",
          "is_child_vaccine_complete": false,
          "is_child_development_normal": null,
          "is_smoke": false,
          "is_drink_alchohol": false,
          "smoke_describe": {},
          "drink_alchohol_describe": {},
          "reference_by": "",
          "is_birth_control": false,
          "birth_control_describe": null
        }
      ],
      "patient_pregnant": [
        {
          "admit_id": "ec4adbad-c71d-43ec-9026-3f565a5e807a",
          "gravida": 1,
          "para": 1,
          "abortion": 1,
          "live_birth": 1,
          "note": ""
        }
      ]
    }
  @ViewChild('canvas') canvasRef?: ElementRef<HTMLCanvasElement>; // Add "?" to make it optional
  private context?: CanvasRenderingContext2D;
  pictures: any;
  //bufferData?: { type: string; data: number[]; } | undefined; // Data received from the server
  imgg: any;
  imageurl: any;

  dataLoaded: boolean = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private admisstionNoteService: AdmisstionNoteService,
    private ngxSpinnerService: NgxSpinnerService,
    private messageService: MessageService,
    private location: Location,
    private domSanitizer: DomSanitizer,
    private http: HttpClient

  ) {
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log('params', this.queryParamsData);
  }

  onChange(result: Date): void {
    console.log('onChange: ', result);
  }
  backPage() {
    this.location.back();
  }
  getValueOrDefault<T>(value: T | undefined | null | '', defaultValue: T | null = null): T | null {
    if (value === undefined || value === null || value === '') {
      return defaultValue;
    }
    return value;
  }
  receiveDataFromChild(data: string) {
    console.log('Data received in parent:', data);
    // Handle the received data here
    this.pictures = data;
  }


  // get patient info on init
  async ngOnInit(){
    let _user = sessionStorage.getItem('userData');
    if (_user != 'undefined' && _user != null) {
      let user = JSON.parse(_user!);
      this.userData = user;
    } else {
      this.userData = null;
    }

    // this.getDoctorOrder();
    await this.getList();
    await this.getAdmissionNote();
    this.buttonSpeedDial();
    await  this.getLabFinding();
    // this.getLabCbc();
  }


 
  show() {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Message Content' });
  }

  // logout
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  // back to home
  onPageIndexChange(pageIndex: any) {
    this.offset = pageIndex === 1 ? 0 : (pageIndex - 1) * this.pageSize;

    //  this.getList();
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize;
    this.pageIndex = 1;

    this.offset = 0;
    //  this.getList();
  }
  async getDoctorOrder() {
    let admitId = this.queryParamsData;
    let datas = await this.admisstionNoteService.getDoctorOrderById(admitId);
    let sortData = _.orderBy(datas.data.data, ["doctor_order_date", "doctor_order_time"], ['desc', 'desc']);
    this.doctorOrder = sortData;
    this.isNewOrder = sortData[0].is_confirm;
    var date2 = DateTime.now();
    var date3 = DateTime.now();
    const date5 = date2.toFormat('yyyy-MM-dd');
    const time1 = date3.toFormat('HH:mm:ss');
    let doctor_order = [{
      "id": sortData[0].id,
      "admit_id": sortData[0].admit_id,
      "doctor_order_date": sortData[0].doctor_order_date,
      "doctor_order_time": sortData[0].doctor_order_time,
      "doctor_order_by": sortData[0].doctor_order_by,
      "is_confirm": sortData[0].is_confirm,
      "is_active": true,
      "create_date": sortData[0].doctor_order_date,
      "create_by": null,
      "modify_date": date5,
      "modify_by": this.userData.id || null,
      "status": "pending",
      "confirm_by": null,
      "confirm_date": null,
      "confirm_time": null
    }]
    this.doctorOrderData = doctor_order;
    console.log(this.doctorOrder);

  }

  // get patient info
  async getList() {
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.admisstionNoteService.getPatientInfo(
        this.queryParamsData
      );

      const data: any = response.data;
      console.log(data);

      // set data to patient info
      this.patientInfo = await data.data;
      console.log(this.patientInfo);
      this.hn = this.patientInfo.hn;
      this.an = this.patientInfo.an;
      this.title = this.patientInfo.patient.title;
      this.fname = this.patientInfo.patient.fname;
      this.lname = this.patientInfo.patient.lname;
      this.gender = this.patientInfo.patient.gender;
      this.age = this.patientInfo.patient.age;
      this.address = this.patientInfo.patient.address;
      this.phone = this.patientInfo.patient.phone;
      this.chief_complaint = this.patientInfo.opd_review.chief_complaint;
      this.pre_diag = this.patientInfo.pre_diag;

      this.present_illness = this.patientInfo.opd_review.present_illness;
      this.past_history = this.patientInfo.opd_review.past_history;
      this.physical_exam = this.patientInfo.opd_review.physical_exam;
      this.body_temperature = this.patientInfo.opd_review.body_temperature;
      this.body_weight = this.patientInfo.opd_review.body_weight;
      this.body_height = this.patientInfo.opd_review.body_height;
      this.waist = this.patientInfo.opd_review.waist;
      this.pulse_rate = this.patientInfo.opd_review.pulse_rate;
      this.respiratory_rate = this.patientInfo.opd_review.respiratory_rate;
      this.systolic_blood_pressure = this.patientInfo.opd_review.systolic_blood_pressure;
      this.diatolic_blood_pressure = this.patientInfo.opd_review.diatolic_blood_pressure;
      this.oxygen_sat = this.patientInfo.opd_review.oxygen_sat;
      this.eye_score = this.patientInfo.opd_review.eye_score;
      this.movement_score = this.patientInfo.opd_review.movement_score;
      this.verbal_score = this.patientInfo.opd_review.verbal_score;

      //this.is_allergy = this.patientInfo.patient_allergy.length > 0 ? true:false;
      // console.log( this.is_allergy);


      /////////////////////////////////


      // split age to year, month, day
      // let age = this.patientInfo.patient.age;
      // let ageSplit = age.split('-');
      // this.age_y = ageSplit[0];
      // this.age_m = ageSplit[1];
      // this.age_d = ageSplit[2];

      // if (Number(this.age_y) == 0) {
      //   this.age = Number(this.age_m) + ' เดือน ' + Number(this.age_d) + ' วัน';
      // } else {
      //   this.age = Number(this.age_y) + ' ปี ';
      // }

      // check if value is zero
      if (this.body_height != 0 && this.body_weight != 0) {
        this.bmi = this.body_weight / ((this.body_height / 100) * (this.body_height / 100));
      } else {
        this.bmi = 0;
      }

      this.total = data.total || 1
      /*
            this.dataSet = data.data.map((v: any) => {
              const date = v.admit_date
                ? DateTime.fromISO(v.admit_date)
                    .setLocale('th')
                    .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
                : '';
              v.admit_date = date;
              return v;
            });*/


    } catch (error: any) {
      console.log(error);
    }
  }


  async getAdmissionNote() {

    try {

      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.admisstionNoteService.getAdmissionNote(
        this.queryParamsData
      );
      const data: any = response.data;
      console.log(data);
      console.log('getAdmissionNote');



      // set data to patient info
      this.admissionNote = await data.data;
      this.dataLoaded = true;


      // this.imgg = this.admissionNote.admission_note[0].pictures.data ? this.admissionNote.admission_note[0].pictures.data : null;
      // Before accessing the data, ensure admissionNote and pictures are not null or undefined
      // if (this.admissionNote?.admission_note?.[0]?.pictures?.data) {
      //   this.bufferData = this.admissionNote.admission_note[0].pictures;
      //   this.imgg = this.admissionNote.admission_note[0].pictures.data;
      //   this.imageurl = String.fromCharCode.apply(String, this.imgg);

      // } else {
      
      //   this.bufferData = { type: 'null', data: [] };
      //   this.imgg = 'default-image-url.jpg'; // Or any other default value
      //   this.imageurl = '';

      // }

      // console.log(this.imageurl);


      if (this.admissionNote.patient_history.length > 0) {
        //    console.log(this.admissionNote.patient_history.length);

        this.is_chronic = this.admissionNote.patient_history[0].is_chronic;
        this.chronic_describe = this.admissionNote.patient_history[0].chronic_describe;

        this.is_allergy = this.admissionNote.patient_history[0].is_allergy;
        this.allergy_describe = this.admissionNote.patient_history[0].allergy_describe;

        this.is_operation = this.admissionNote.patient_history[0].is_operation;
        this.operation_describe = this.admissionNote.patient_history[0].operation_describe;

        this.smoke_describe_duration = this.admissionNote.patient_history[0].smoke_describe.duration;
        this.smoke_describe_qty = this.admissionNote.patient_history[0].smoke_describe.qty;
        this.smoke_describe_frequency = this.admissionNote.patient_history[0].smoke_describe.frequency;


        this.drink_alchohol_describe_duration = this.admissionNote.patient_history[0].drink_alchohol_describe.duration;
        this.drink_alchohol_describe_qty = this.admissionNote.patient_history[0].drink_alchohol_describe.qty;
        this.drink_alchohol_describe_frequency = this.admissionNote.patient_history[0].drink_alchohol_describe.frequency;


        this.is_smoke = this.admissionNote.patient_history[0].is_smoke;
        this.is_drink_alchohol = this.admissionNote.patient_history[0].is_drink_alchohol;
        this.is_family_disease = this.admissionNote.patient_history[0].is_family_disease;
        this.family_disease_describe = this.admissionNote.patient_history[0].family_disease_describe;
        this.is_child_vaccine_complete = this.admissionNote.patient_history[0].is_child_vaccine_complete;
        this.is_child_development_normal = this.admissionNote.patient_history[0].is_child_development_normal;


      
        this.heent = this.admissionNote.admission_note[0].physical_examination ? this.admissionNote.admission_note[0].physical_examination.heent : '';     
        
        this.heart = this.admissionNote.admission_note[0].physical_examination ? this.admissionNote.admission_note[0].physical_examination.heart : '';
        this.lung = this.admissionNote.admission_note[0].physical_examination ? this.admissionNote.admission_note[0].physical_examination.lung : '';

        this.abdomen = this.admissionNote.admission_note[0].physical_examination ? this.admissionNote.admission_note[0].physical_examination.abdomen : '';
        this.back_and_cva = this.admissionNote.admission_note[0].physical_examination ? this.admissionNote.admission_note[0].physical_examination.back_and_cva : '';
        this.extremities_and_skin = this.admissionNote.admission_note[0].physical_examination ? this.admissionNote.admission_note[0].physical_examination.extremities_and_skin : '';
        this.neuro_signs = this.admissionNote.admission_note[0].physical_examination ? this.admissionNote.admission_note[0].physical_examination.neuro_signs : '';
        this.general_appearance = this.admissionNote.admission_note[0].physical_examination ? this.admissionNote.admission_note[0].physical_examination.general_appearance : '';
        this.note = this.admissionNote.admission_note[0].physical_examination ? this.admissionNote.admission_note[0].physical_examination.note : '';




        this.pheent = this.admissionNote.admission_note[0].review_of_system ? this.admissionNote.admission_note[0].review_of_system.heent : '';
        this.pheart = this.admissionNote.admission_note[0].review_of_system ? this.admissionNote.admission_note[0].review_of_system.heart : '';
        this.plung = this.admissionNote.admission_note[0].review_of_system ? this.admissionNote.admission_note[0].review_of_system.lung : '';
        this.pabdomen = this.admissionNote.admission_note[0].review_of_system ? this.admissionNote.admission_note[0].review_of_system.abdomen : '';
        this.pback_and_cva = this.admissionNote.admission_note[0].review_of_system ? this.admissionNote.admission_note[0].review_of_system.back_and_cva : '';
        this.pextremities_and_skin = this.admissionNote.admission_note[0].review_of_system ? this.admissionNote.admission_note[0].review_of_system.extremities_and_skin : '';
        this.pneuro_signs = this.admissionNote.admission_note[0].review_of_system ? this.admissionNote.admission_note[0].review_of_system.neuro_signs : '';
        this.pgeneral_appearance = this.admissionNote.admission_note[0].review_of_system ? this.admissionNote.admission_note[0].review_of_system.general_appearance : '';
        this.plan_of_treatment = this.admissionNote.admission_note[0].plan_of_treatment ? this.admissionNote.admission_note[0].plan_of_treatment : '';
        this.diagnosis = this.admissionNote.admission_note[0].pre_diagnosis ? this.admissionNote.admission_note[0].pre_diagnosis : '';


        // this.pheent = this.admissionNote.admission_note[0].review_of_system.heent;
        // this.pheart = this.admissionNote.admission_note[0].review_of_system.heart;
        // this.plung = this.admissionNote.admission_note[0].review_of_system.lung;
        // this.pabdomen = this.admissionNote.admission_note[0].review_of_system.abdomen;
        // this.pback_and_cva = this.admissionNote.admission_note[0].review_of_system.back_and_cva;
        // this.pextremities_and_skin = this.admissionNote.admission_note[0].review_of_system.extremities_and_skin;
        // this.pneuro_signs = this.admissionNote.admission_note[0].review_of_system.neuro_signs;
        // this.pgeneral_appearance = this.admissionNote.admission_note[0].review_of_system.general_appearance;

        // this.plan_of_treatment = this.admissionNote.admission_note[0].plan_of_treatment;
        // this.diagnosis = this.admissionNote.admission_note[0].pre_diagnosis;

        // pregnant  
        // this.is_birth_control = this.admissionNote.patient_history[0].is_birth_control ?? null;
        this.is_birth_control = this.getValueOrDefault(this.admissionNote.patient_history[0]?.is_birth_control);


        //console.log( datePipe.transform( this.admissionNote.patient_history[0].last_menstrual_period),formatDate));

        //this.last_menstrual_period = this.admissionNote.patient_history[0].last_menstrual_period;
        let date: any = new Date(this.admissionNote.patient_history[0].last_menstrual_period);
        date = date.getTime() + (7 * 60 * 60 * 1000);
        let thai_date = new Date(date);
        this.last_menstrual_period = thai_date.toISOString().split('T')[0];
        this.para = this.getValueOrDefault(this.admissionNote.patient_pregnant[0]?.para);
        this.gravida = this.getValueOrDefault(this.admissionNote.patient_pregnant[0]?.gravida);
        this.abortion = this.getValueOrDefault(this.admissionNote.patient_pregnant[0]?.abortion);
        this.live_birth = this.getValueOrDefault(this.admissionNote.patient_pregnant[0]?.live_birth);
        // this.gestational_age = this.getValueOrDefault(this.admissionNote.patient_pregnant[0]?.gestational_age);


      } else {

        this.queryParamsData

      }


    } catch (error: any) {

      console.log(error);
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาดx1 !',
        detail: error,
      });

    }
  }






  // save admission note
  async saveAdmissionNote() {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0'); // Month is zero-based, so add 1
    const day = String(currentDate.getDate()).padStart(2, '0');
  
    console.log('year:'+parseInt(this.age.split('-')[0]))
    const formattedDate = `${year}-${month}-${day}`;

    let data: any;
    let isvalid: boolean = true;
    ///กรณีเป็นผู้ป่วยชาย
    if (this.gender == "หญิง" && parseInt(this.age.split('-')[0]) > 8) {
      
      if (!this.requirefield_birth_control()) {
        isvalid = false;
        this.messageService.add({
          severity: 'error',
          summary: 'เกิดข้อผิดพลาด !',
          detail: "กรุณากรอกข้อมูลการตั้งครรภ์",
        });
        //  return false;
      }
      // วันที่คุมกำเนิด
      if (this.last_menstrual_period) {
        console.log("fon");
        let date: any = new Date(this.last_menstrual_period);
        date = date.getTime() + (7 * 60 * 60 * 1000);
        let thai_date = new Date(date);
        this.last_menstrual_period = thai_date.toISOString().split('T')[0];
      } else {
        this.last_menstrual_period = null;

      }

      if (this.admissionNote.patient_history.length > 0) {
        // update admissionNote
        // console.log("fonfon");

        data = {
          admission_note: [{
            admit_id: this.queryParamsData,
            id: this.admissionNote.admission_note[0].id,
            pre_diagnosis: this.diagnosis,
            plan_of_treatment: this.plan_of_treatment,
            pictures: this.pictures ? this.pictures : null,
            physical_examination: {
              heent: this.heent,
              heart: this.heart,
              lung: this.lung,
              abdomen: this.abdomen,
              back_and_cva: this.back_and_cva,
              extremities_and_skin: this.extremities_and_skin,
              neuro_signs: this.neuro_signs,
              general_appearance: this.general_appearance,
              note: this.note,
            },
            review_of_system: {
              heent: this.pheent,
              heart: this.pheart,
              lung: this.plung,
              abdomen: this.pabdomen,
              back_and_cva: this.pback_and_cva,
              extremities_and_skin: this.pextremities_and_skin,
              neuro_signs: this.pneuro_signs,
              general_appearance: this.pgeneral_appearance,
            },
          }],
          patient_history: [{
            admit_id: this.queryParamsData,
            is_chronic: this.is_chronic,
            chronic_describe: this.chronic_describe,
            is_allergy: this.is_allergy,
            allergy_describe: this.allergy_describe,
            is_operation: this.is_operation,
            operation_describe: this.operation_describe,
            is_family_disease: this.is_family_disease,
            family_disease_describe: this.family_disease_describe,
            last_menstrual_period: this.last_menstrual_period,
            is_child_development_normal: this.is_child_development_normal,
            is_child_vaccine_complete: this.is_child_vaccine_complete,
            is_smoke: this.is_smoke,
            is_drink_alchohol: this.is_drink_alchohol,
            is_birth_control: this.is_birth_control,
            smoke_describe: {
              duration: this.smoke_describe_duration,
              qty: this.smoke_describe_qty,
              // frequency: this.smoke_describe_frequency,
              // frequency: 1,
            },
            drink_alchohol_describe: {
              duration: this.drink_alchohol_describe_duration,
              qty: this.drink_alchohol_describe_qty,
              frequency: this.drink_alchohol_describe_frequency,
            },
            reference_by: '',
            modify_date: formattedDate,
          }],
          patient_pregnant: [{
            admit_id: this.queryParamsData,
            gravida: this.gravida,
            para: this.para,
            abortion: this.abortion,
            live_birth: this.live_birth,
          }],
        };
      } else {
        //Insert admissionNote
        data = {
          admission_note: [{
            admit_id: this.queryParamsData,
            pre_diagnosis: this.diagnosis,
            plan_of_treatment: this.plan_of_treatment,
            pictures: this.pictures ? this.pictures : null,
            physical_examination: {
              heent: this.heent,
              heart: this.heart,
              lung: this.lung,
              abdomen: this.abdomen,
              back_and_cva: this.back_and_cva,
              extremities_and_skin: this.extremities_and_skin,
              neuro_signs: this.neuro_signs,
              general_appearance: this.general_appearance,
              note: this.note,
            },
            review_of_system: {
              heent: this.pheent,
              heart: this.pheart,
              lung: this.plung,
              abdomen: this.pabdomen,
              back_and_cva: this.pback_and_cva,
              extremities_and_skin: this.pextremities_and_skin,
              neuro_signs: this.pneuro_signs,
              general_appearance: this.pgeneral_appearance,
            },
          }],
          patient_history: [{
            admit_id: this.queryParamsData,
            is_chronic: this.is_chronic,
            chronic_describe: this.chronic_describe,
            is_allergy: this.is_allergy,
            allergy_describe: this.allergy_describe,
            is_operation: this.is_operation,
            operation_describe: this.operation_describe,
            is_family_disease: this.is_family_disease,
            family_disease_describe: this.family_disease_describe,
            last_menstrual_period: this.last_menstrual_period,
            is_child_development_normal: this.is_child_development_normal,
            is_child_vaccine_complete: this.is_child_vaccine_complete,
            is_smoke: this.is_smoke,
            is_drink_alchohol: this.is_drink_alchohol,
            is_birth_control: this.is_birth_control,
            smoke_describe: {
              duration: this.smoke_describe_duration,
              qty: this.smoke_describe_qty,
              // frequency: this.smoke_describe_frequency,
              // frequency: 1,
            },
            drink_alchohol_describe: {
              duration: this.drink_alchohol_describe_duration,
              qty: this.drink_alchohol_describe_qty,
              frequency: this.drink_alchohol_describe_frequency,
            },
            reference_by: '',
            modify_date: formattedDate,
          }],
          patient_pregnant: [{
            admit_id: this.queryParamsData,
            gravida: this.gravida,
            para: this.para,
            abortion: this.abortion,
            live_birth: this.live_birth,
            // gestational_age:  this.gestational_age,
            // note:  '',
          }],
        };

      }


    } else {

      /// กรณีเป็นผู้ป่วยชาย
      if (this.admissionNote.patient_history.length > 0) {
        //update
        data = {
          admission_note: [{
            admit_id: this.queryParamsData,
            id: this.admissionNote.admission_note[0].id,
            pre_diagnosis: this.diagnosis,
            plan_of_treatment: this.plan_of_treatment,
            pictures: this.pictures ? this.pictures : null,
            physical_examination: {
              heent: this.heent,
              heart: this.heart,
              lung: this.lung,
              abdomen: this.abdomen,
              back_and_cva: this.back_and_cva,
              extremities_and_skin: this.extremities_and_skin,
              neuro_signs: this.neuro_signs,
              general_appearance: this.general_appearance,
              note: this.note,
            },
            review_of_system: {
              heent: this.pheent,
              heart: this.pheart,
              lung: this.plung,
              abdomen: this.pabdomen,
              back_and_cva: this.pback_and_cva,
              extremities_and_skin: this.pextremities_and_skin,
              neuro_signs: this.pneuro_signs,
              general_appearance: this.pgeneral_appearance,
            },
          }],
          patient_history: [{
            admit_id: this.queryParamsData,
            is_chronic: this.is_chronic,
            chronic_describe: this.chronic_describe,
            is_allergy: this.is_allergy,
            allergy_describe: this.allergy_describe,
            is_operation: this.is_operation,
            operation_describe: this.operation_describe,
            is_family_disease: this.is_family_disease,
            family_disease_describe: this.family_disease_describe,
            last_menstrual_period: this.last_menstrual_period,
            is_child_development_normal: this.is_child_development_normal,
            is_child_vaccine_complete: this.is_child_vaccine_complete,
            is_smoke: this.is_smoke,
            is_drink_alchohol: this.is_drink_alchohol,
            is_birth_control: this.is_birth_control,
            smoke_describe: {
              duration: this.smoke_describe_duration,
              qty: this.smoke_describe_qty,
              // frequency: this.smoke_describe_frequency,
              // frequency: 1,
            },
            drink_alchohol_describe: {
              duration: this.drink_alchohol_describe_duration,
              qty: this.drink_alchohol_describe_qty,
              frequency: this.drink_alchohol_describe_frequency,
            },
            reference_by: '',
            modify_date: formattedDate
          }],
        };
      } else {
        // insert // remove   id:this.admissionNote.admission_note[0].id,
        data = {
          admission_note: [{
            admit_id: this.queryParamsData,
            pre_diagnosis: this.diagnosis,
            plan_of_treatment: this.plan_of_treatment,
            pictures: this.pictures ? this.pictures : null,
            physical_examination: {
              heent: this.heent,
              heart: this.heart,
              lung: this.lung,
              abdomen: this.abdomen,
              back_and_cva: this.back_and_cva,
              extremities_and_skin: this.extremities_and_skin,
              neuro_signs: this.neuro_signs,
              general_appearance: this.general_appearance,
              note: this.note,
            },
            review_of_system: {
              heent: this.pheent,
              heart: this.pheart,
              lung: this.plung,
              abdomen: this.pabdomen,
              back_and_cva: this.pback_and_cva,
              extremities_and_skin: this.pextremities_and_skin,
              neuro_signs: this.pneuro_signs,
              general_appearance: this.pgeneral_appearance,
            },
          }],
          patient_history: [{
            admit_id: this.queryParamsData,
            is_chronic: this.is_chronic,
            chronic_describe: this.chronic_describe,
            is_allergy: this.is_allergy,
            allergy_describe: this.allergy_describe,
            is_operation: this.is_operation,
            operation_describe: this.operation_describe,
            is_family_disease: this.is_family_disease,
            family_disease_describe: this.family_disease_describe,
            last_menstrual_period: this.last_menstrual_period,
            is_child_development_normal: this.is_child_development_normal,
            is_child_vaccine_complete: this.is_child_vaccine_complete,
            is_smoke: this.is_smoke,
            is_drink_alchohol: this.is_drink_alchohol,
            is_birth_control: this.is_birth_control,
            smoke_describe: {
              duration: this.smoke_describe_duration,
              qty: this.smoke_describe_qty,
              // frequency: this.smoke_describe_frequency,
              // frequency: 1,
            },
            drink_alchohol_describe: {
              duration: this.drink_alchohol_describe_duration,
              qty: this.drink_alchohol_describe_qty,
              frequency: this.drink_alchohol_describe_frequency,
            },
            reference_by: '',
            modify_date: formattedDate
          }],
        };
      }


    }

    // console.log(data);
    //  console.log(this.templates);

    if (isvalid) {
      try {

        let rs = await this.admisstionNoteService.saveAdmissionNote(data);
        console.log(rs);

        this.messageService.add({
          severity: 'success',
          summary: 'บันทึกสำเร็จ #',
          detail: 'รอสักครู่...',
        });
        
        this.router.navigate(['/list-patient']);

      } catch (error: any) {
        console.log("saveAdmissionNote() error=" + error);
        this.messageService.add({
          severity: 'error',
          summary: 'เกิดข้อผิดพลาด !',
          detail: 'กรูณาตรวจสอบ+++',
        });

      }
    }

  }
  async saveAdmissionNotePh() {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0'); // Month is zero-based, so add 1
    const day = String(currentDate.getDate()).padStart(2, '0');

    const formattedDate = `${year}-${month}-${day}`;
    // console.log(formattedDate);
    let data: any;
    // set data to admission note
    let datat: any = {
      admission_note: [],
      patient_history: [{
        admit_id: this.queryParamsData,
        is_chronic: this.is_chronic,
        chronic_describe: this.chronic_describe,
        birth_control_describe: "",
        // is_allergy:this.is_allergy,
        allergy_describe: this.allergy_describe,
        is_operation: this.is_operation,
        operation_describe: this.operation_describe,
        is_family_disease: this.is_family_disease,
        family_disease_describe: this.family_disease_describe,
        last_menstrual_period: '2023-09-12',
        is_child_development_normal: this.is_child_development_normal,
        is_child_vaccine_complete: this.is_child_vaccine_complete,
        is_smoke: this.is_smoke,
        is_drink_alchohol: this.is_drink_alchohol,
        smoke_describe: {
          duration: this.smoke_describe_duration,
          qty: this.smoke_describe_qty,
          frequency: this.smoke_describe_frequency,
        },
        drink_alchohol_describe: {
          duration: this.drink_alchohol_describe_duration,
          qty: this.drink_alchohol_describe_qty,
          frequency: this.drink_alchohol_describe_frequency,
        },
        reference_by: '',
        modify_date: '2023-09-12',
      }],
      patient_pregnant: [],
    };



    // console.log(datat);
    // console.log(this.templates);

    try {
      let rs = await this.admisstionNoteService.saveAdmissionNote(datat);
      console.log(rs);

      this.messageService.add({
        severity: 'success',
        summary: 'บันทึกสำเร็จ #',
        detail: 'รอสักครู่...',
      });
      this.resetForm();
    } catch (error: any) {
      console.log(error);
      this.messageService.add({
        severity: 'error',
        summary: 'เกิดข้อผิดพลาด !',
        detail: 'กรูณาตรวจสอบ',
      });

    }
  }

  // reset form input 
  resetForm() {
    this.chronic_describe = '';
    this.allergy_describe = '';
    this.operation_describe = '';
    this.family_disease_describe = '';
    this.last_menstrual_period = '';
    this.smoke_describe = '';
    this.smoke_describe_duration = '';
    this.smoke_describe_qty = '';
    this.smoke_describe_frequency = '';
    this.drink_alchohol_describe = '';
    this.drink_alchohol_describe_duration = '';
    this.drink_alchohol_describe_qty = '';
    this.drink_alchohol_describe_frequency = '';
    this.gravida = '';
    this.para = '';
    this.abortion = '';
    this.live_birth = '';
    this.is_allergy = false;
    this.is_operation = false;
    this.is_family_disease = false;
    this.is_child_vaccine_complete = false;
    this.is_smoke = false;
    this.is_drink_alchohol = false;
    this.is_child_development_normal = false;
    this.heent = false;
    this.heart = false;
    this.lung = false;
    this.abdomen = false;
    this.back_and_cva = false;
    this.extremities_and_skin = false;
    this.neuro_signs = false;
    this.general_appearance = false;

    this.pheent = false;
    this.pheart = false;
    this.plung = false;
    this.pabdomen = false;
    this.pback_and_cva = false;
    this.pextremities_and_skin = false;
    this.pneuro_signs = false;
    this.pgeneral_appearance = false;
    this.ngOnInit();
  }

  buttonSpeedDial() {
    this.speedDialitems = [
      {
        icon: 'pi pi-arrow-left',
        routerLink: ['/list-patient'],
        tooltipOptions: {
          tooltipLabel: 'ย้อนกลับ',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-user-doctor',
        command: () => {
          this.navigateDoctorOrder()
        },
        tooltipOptions: {
          tooltipLabel: 'Doctor Order',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-circle-info',
        command: () => {
          this.navigatePatientInfo()
        },
        tooltipOptions: {
          tooltipLabel: 'Patient Info',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-heart-pulse',
        command: () => {
          this.navigateEkg()
        },
        tooltipOptions: {
          tooltipLabel: 'EKG',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-phone-volume',
        command: () => {
          this.navigateConsult()
        },
        tooltipOptions: {
          tooltipLabel: 'Consult',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-vial-virus',
        command: () => {
          this.navigateLab()
        },
        tooltipOptions: {
          tooltipLabel: 'Lab',
          tooltipPosition: 'bottom'
        },
      }
    ];
  }

  navigateDoctorOrder() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log('admit id:', jsonString);
    this.router.navigate(['/doctor-order'], { queryParams: { data: jsonString } });

  }

  navigateEkg() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);
    this.router.navigate(['/ekg'], { queryParams: { data: jsonString } });

  }

  navigatePatientInfo() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);
    this.router.navigate(['/patient-info'], { queryParams: { data: jsonString } });

  }

  navigateLab() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);
    this.router.navigate(['/lab'], { queryParams: { data: jsonString } });

  }

  navigateConsult() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);
    this.router.navigate(['/consult'], { queryParams: { data: jsonString } });

  }
  testme() {
    console.log(this.heent);
  }
  click_is_allergy_N() {
    // console.log("click_is_allergy");

    this.allergy_describe = "";

  }
  click_is_operation_N() {
    // console.log("click_is_allergy");

    this.operation_describe = "";

  }

  click_is_chronic_N() {
    // console.log("click_is_allergy");
    this.chronic_describe = "";
  }
  click_is_family_disease_N() {
    // console.log("click_is_allergy");
    this.family_disease_describe = "";
  }

  click_is_smoke_N() {
    this.smoke_describe_qty = "";
    this.smoke_describe_duration = "";

  }
  click_is_drink_alchohol_N() {
    this.drink_alchohol_describe_duration = "";
    this.drink_alchohol_describe_qty = "";
    this.drink_alchohol_describe_frequency = "";
  }

  requirefield_birth_control() {
    console.log("xx1");
    console.log(this.gravida);
    // || !this.gravida || !this.para || !this.live_birth
    if (!this.last_menstrual_period ) {
      return false;

    } else {
      return true;

    }
  }
  cbc: any;
  async getLabCbc() {
    console.log('getLabCbc');
    try {
      let rs = await this.admisstionNoteService.getLabCbc(this.an, '001');
      console.log(rs);
      this.cbc = rs.data;
    } catch (error: any) {
      console.log(error);


    }
  }
  labFinding: any;

  async getLabFinding() {
    // http://209.15.114.75/api-nurse/nurse/his-services/waiting-lab-finding?an=66000124
    console.log('getLabFinding');
    try {
      let rs:any = await this.admisstionNoteService.getLabFinding(this.an);
      console.log('lab:',rs.data);
      this.labFinding = rs.data;
      let cbc = rs.data.filter((x:any)=> x.name == 'CBC');

      if(cbc){
        this.is_cbc = true;
        this.hb = cbc.find((x:any)=> x.lab_test_name == 'HB').lab_test_result;
        this.hct = cbc.find((x:any)=> x.lab_test_name == 'HCT').lab_test_result;
        this.wbc = cbc.find((x:any)=> x.lab_test_name == 'WBC').lab_test_result;
        this.plt = cbc.find((x:any)=> x.lab_test_name == 'PLTC').lab_test_result;
        this.pmn = cbc.find((x:any)=> x.lab_test_name == 'PMN').lab_test_result;
        this.lymph = cbc.find((x:any)=> x.lab_test_name == 'LYM').lab_test_result;
      }

      let elyte = rs.data.filter((x:any)=> x.name == 'ELECTROLYTE');
      if(elyte){
        this.is_elyte = true;
        this.na = elyte.find((x:any)=> x.lab_test_name == 'NA').lab_test_result;
        this.k = elyte.find((x:any)=> x.lab_test_name == 'K').lab_test_result;
        this.cl = elyte.find((x:any)=> x.lab_test_name == 'CL').lab_test_result;
        this.co2 = elyte.find((x:any)=> x.lab_test_name == 'CO2').lab_test_result;
      }
    } catch (error: any) {
      console.log(error);


    }
  }
}

