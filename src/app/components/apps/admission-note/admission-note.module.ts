import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {NgxSpinnerModule} from 'ngx-spinner'
import { AdmissionNoteRoutingModule } from './admission-note-routing.module';
import { AdmissionNoteComponent } from './admission-note.component'
import { ToastModule } from 'primeng/toast';
import { SharedModule } from '../../../shared/sharedModule';

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PanelModule } from 'primeng/panel';
import { SplitterModule } from 'primeng/splitter';
import { CheckboxModule } from 'primeng/checkbox';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule} from 'primeng/button';
import { RippleModule } from 'primeng/ripple';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CalendarModule } from 'primeng/calendar';
import {RadioButtonModule} from 'primeng/radiobutton';
import { SpeedDialModule } from 'primeng/speeddial';
import { InputMaskModule } from 'primeng/inputmask';
import { InputNumberModule } from 'primeng/inputnumber';
import { DatePipe } from '@angular/common';
import { AvatarModule } from 'primeng/avatar';
import {DrawingCanvasComponent} from './drawing-canvas/drawing-canvas.component';
import { AnatomyDrawingComponent } from './anatomy-drawing/anatomy-drawing.component';
import {CanvasWhiteboardModule} from 'ng2-canvas-whiteboard'





@NgModule({
    declarations: [
      AdmissionNoteComponent,DrawingCanvasComponent, AnatomyDrawingComponent
    ],
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        AdmissionNoteRoutingModule, NgxSpinnerModule, MessagesModule, MessageModule, SharedModule, PanelModule, SplitterModule, CheckboxModule, RadioButtonModule,
        TableModule, InputTextModule, RippleModule, ButtonModule, InputTextareaModule, CalendarModule, SpeedDialModule,ToastModule,InputMaskModule,InputNumberModule,DatePipe,
        AvatarModule,CanvasWhiteboardModule
 
    ]
})
export class AdmissionNoteModule { }