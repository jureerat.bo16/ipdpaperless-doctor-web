import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-drawing-canvas',
  templateUrl: './drawing-canvas.component.html',
  styleUrls: ['./drawing-canvas.component.scss']
})
export class DrawingCanvasComponent implements AfterViewInit {
  @ViewChild('canvas') canvasRef?: ElementRef<HTMLCanvasElement>;
  public context: CanvasRenderingContext2D | null = null;
  public backgroundImageLoaded: boolean = false;
  public isDrawing: boolean = false;
  public lastX: number = 0;
  public lastY: number = 0;
  public penColor: string = '#FF0000'; // Default color is red
  public lineWidth: number = 2; // Default line width
  public tool: string = 'pen'; // Default tool is pen
  private drawnPaths: Path2D[] = [];

  ngAfterViewInit(): void {
    const canvas:any = this.canvasRef?.nativeElement;
    // canvas.width = 200; // Set the width of the canvas
    canvas.height = 200; // Set the height of the canvas
    if (canvas) {
      this.context = canvas.getContext('2d');
      if (!this.context) {
        throw new Error('Failed to get 2D context.');
      }

      const img = new Image();
      img.onload = () => {
        // Draw the background image once it's loaded with custom height
        const imageHeight = 200; // Specify the desired height of the image
        this.context?.drawImage(img, 0, 0, canvas.width, imageHeight);
        // Set the flag to indicate that the background image is loaded
        this.backgroundImageLoaded = true;
      };
      img.src = '../../../../assets/images/body.jpg'; // Replace with your background image path

      // Set up drawing events
      this.captureMouseEvents(canvas);
    }
  }

  public captureMouseEvents(canvas: HTMLCanvasElement): void {
    // Touch event listeners
    canvas.addEventListener('mousedown', (e) => {
      if (!this.context || !this.backgroundImageLoaded) return;
      this.isDrawing = true;
      const rect = canvas.getBoundingClientRect();
      this.lastX = e.clientX - rect.left;
      this.lastY = e.clientY - rect.top;
      this.draw(this.lastX, this.lastY);
    });

    canvas.addEventListener('mousemove', (e) => {
      if (!this.context || !this.isDrawing || !this.backgroundImageLoaded) return;
      const rect = canvas.getBoundingClientRect();
      const x = e.clientX - rect.left;
      const y = e.clientY - rect.top;
      console.log('x:' +x);
      console.log('x:' +y);
      this.draw(x, y);
    });

    canvas.addEventListener('mouseup', () => {
      this.isDrawing = false;
    });

    canvas.addEventListener('mouseleave', () => {
      this.isDrawing = false;
    });

    canvas.addEventListener('touchstart', (e) => {
      if (!this.context || !this.backgroundImageLoaded) return;
      this.isDrawing = true;
      const touch = e.touches[0];
      const rect = canvas.getBoundingClientRect();
      this.lastX = touch.clientX - rect.left;
      this.lastY = touch.clientY - rect.top;
      this.draw(this.lastX, this.lastY);
      // Prevent default touch event behavior (like scrolling or zooming)
      e.preventDefault();
    });

    canvas.addEventListener('touchmove', (e) => {
      if (!this.context || !this.isDrawing || !this.backgroundImageLoaded) return;
      const touch = e.touches[0];
      const rect = canvas.getBoundingClientRect();
      const x = touch.clientX - rect.left;
      const y = touch.clientY - rect.top;
      this.draw(x, y);
      // Prevent default touch event behavior (like scrolling or zooming)
      e.preventDefault();
    });

    canvas.addEventListener('touchend', () => {
      this.isDrawing = false;
    });


  }

  public draw(x: number, y: number): void {
    if (!this.context) return;
  
    // Set the line width before drawing
    this.context.lineWidth = this.lineWidth;
  
    // Calculate canvas relative coordinates
    const rect = this.canvasRef?.nativeElement.getBoundingClientRect();
    const canvasX = x - rect!.left;
    const canvasY = y - rect!.top;
  
    this.context.beginPath();
    this.context.moveTo(this.lastX, this.lastY);
    this.context.lineTo(canvasX, canvasY);
    this.context.strokeStyle = this.penColor;
    this.context.stroke();
    [this.lastX, this.lastY] = [canvasX, canvasY];
  
    // Save the drawn path
    const path = new Path2D();
    path.moveTo(this.lastX, this.lastY);
    path.lineTo(canvasX, canvasY);
    this.drawnPaths.push(path);
  }
  

  switchTool(tool: string): void {
    this.tool = tool;
  }

  changeColor(color: any): void {
    console.log(color);
    this.penColor = color;
  }

  changeLineWidth(width: number): void {
    this.lineWidth = width;
  }

  clearCanvas(): void {
    const canvas:any = this.canvasRef?.nativeElement;
    // canvas.width = 200; // Set the width of the canvas
    canvas.height = 200; // Set the height of the canvas
    if (canvas && this.context && this.backgroundImageLoaded) {
      this.context.clearRect(0, 0, canvas.width, canvas.height); // Clear the canvas

      // Redraw the background image
      const img = new Image();
      img.onload = () => {
        this.context?.drawImage(img, 0, 0, canvas.width, canvas.height);
      };
      img.src = '../../../../assets/images/body.jpg'; // Replace with your background image path
    }
  }


  saveImage(): void {
    const canvas = this.canvasRef?.nativeElement;
    if (canvas && this.context && this.backgroundImageLoaded) {
      const imageData = canvas.toDataURL('image/png');
      const link = document.createElement('a');
      link.download = 'drawing.png';
      link.href = imageData;
      link.click();
    }
  }
  public undo(): void {
    if (this.drawnPaths.length === 0) return;

    // Clear the canvas
    const canvas = this.canvasRef?.nativeElement;
    if (canvas && this.context && this.backgroundImageLoaded) {
      this.context.clearRect(0, 0, canvas.width, canvas.height);

      // Redraw the background image
      const img = new Image();
      img.onload = () => {
        this.context?.drawImage(img, 0, 0, canvas.width, canvas.height);
      };
      img.src = '../../../../assets/images/body.jpg'; // Replace with your background image path
    }

    // Remove the last drawn path
    this.drawnPaths.pop();

    // Redraw the remaining paths
    this.drawnPaths.forEach(path => {
      this.context?.stroke(path);
    });
  }
}
