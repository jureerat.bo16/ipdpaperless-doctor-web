import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EkgService {

  pathPrefixLookup: any = `api-lookup/lookup`
  pathPrefixDoctor: any = `api-doctor/doctor`
  pathPrefixAuth: any = `api-auth/auth`
  pathPreficNure:any = `api-nurse/nurse`

  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixDoctor}`
  })

  private axiosLookup = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
  })
  private axiosEkg = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPreficNure}`
  })

  constructor() 
  {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });
    this.axiosEkg.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })

    this.axiosLookup.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosLookup.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })
  }
  
  async getEkg(ans:any) {
    
    console.log("ans :",ans);
    const url = `/his-services/waiting-ekg?an=${ans}`

    console.log('ekg:',url);
    // console.log(this.axiosEkg.get(url));
    return await this.axiosEkg.get(url)
  }
  async getPatientInfo(admitId: any) {
    // console.log(admitId);

    const url = `/admit/${admitId}/patient-info`
    // console.log(url);

    return await this.axiosInstance.get(url)
  }

}
