import { Component, Input, OnInit } from '@angular/core';
import { DoctorOrderService } from '../doctor-order-service';
import { LookupService } from '../../../../shared/lookup-service';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import * as _ from 'lodash';
import {SharedDataService} from '../shrare-data-service';

interface AutoCompleteCompleteEvent {
    originalEvent: Event;
    query: string;
}

interface Option {
    label: string;
    name: string;
    id: string;
    code: string;
    default_usage: string;
}

interface OptionItems {
    id: string;
    name: string;
    default_usage: string;
    item_type_id: string;
}

interface OptionDrugsUsage {
    id: string;
    code: string;
    name: string;
    is_active: boolean;
    description: string;
    create_date?: string;
    create_by?: string;
    modify_date?: string;
    modify_by?: string;
    time_per_day?: string;
}
@Component({
    selector: 'app-order-oneday',
    templateUrl: './order-oneday.component.html',
    styleUrls: ['./order-oneday.component.scss'],
})
export class OrderOnedayComponent implements OnInit {
    @Input() parentData: any;
    
    data: string = '';
    sidebarVisible2: boolean = false;
    queryParamsData: any;
    dxData: any;
    pageSize = 20;
    pageIndex = 1;
    offset: any;
    total: any;
    dataSet: any;
    showDx: boolean = true;
    dxName: any;

    orderOnedayV: any;
    orderOndayValue: any = [];

    selectedItems: any;
    filteredItems: any | undefined;

    selectedUsage: any = '';
    filteredUsage: any | undefined;

    selectedQuantity: any = '';
    filteredQuantity: any | undefined;

    subjective: any;
    objective: any;
    assertment: any;
    plan: any;
    note: any;

    progress_note: any;
    progress_note_subjective: any[] = [];
    progress_note_objective: any[] = [];
    progress_note_assertment: any[] = [];
    progress_note_plan: any[] = [];

    patientList: any;
    orderList: any;
    drugList: any;

    orders: any[] = [];
    optionsDrugs: OptionItems[] = [];
    optionsAllItems: OptionItems[] = [];

    optionsDrugsUsage: OptionDrugsUsage[] = [];

    // doctor order id
    doctorOrderData: any;
    is_new_order: boolean = true;
    is_success: boolean = false;

    // user
    userData: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private lookupService: LookupService,
        private doctorOrderService: DoctorOrderService,
        private sharedDataService: SharedDataService

    ) {
        let jsonString: any =
            this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        this.queryParamsData = jsonObject;

        let _data = sessionStorage.getItem('doctor_order');  
        if (_data != 'undefined' && _data != null) {
            let _doctorOrderData = JSON.parse(_data);
            this.doctorOrderData = _doctorOrderData.doctor_order;
            console.log(this.doctorOrderData);
            this.is_new_order = false;
        } else {
            this.is_new_order = true;
        }

        let _user = sessionStorage.getItem('userData');
        if (_user != 'undefined' && _user != null) {
            let user = JSON.parse(_user!);
            this.userData = user;
        } else {
            this.userData = null;
        }
    }

    ngOnInit(): void {
        if(this.parentData != null || this.parentData != undefined){
            this.is_new_order = this.parentData.isNewOrder;
            this.doctorOrderData = this.parentData.doctor_order;
        }
        this.getItem();
        this.getMedicineUsage();
    }

    // get medicine usage
    async getMedicineUsage() {
        try {
            let response = await this.doctorOrderService.getMedusage();
            this.optionsDrugsUsage = response.data.data;
            // console.log('med usage:', this.optionsDrugsUsage);
        } catch (error: any) {
            console.log(error);
        }
    }

    // get list all items
    async getItem() {
        try {
            let response = await this.doctorOrderService.getLookupItem();
            this.optionsAllItems = response.data.data;
            // console.log('Items:', this.optionsAllItems);
        } catch (error: any) {
            console.log(error);
        }
    }

    filterItems(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();

        for (let i = 0; i < this.optionsAllItems.length; i++) {
            let item = this.optionsAllItems[i];
            if (item.name.toLowerCase().indexOf(query) !== -1) {
                filtered.push(item);
            }
        }
        this.filteredItems = filtered;
    }

    filterUsage(event: AutoCompleteCompleteEvent) {
        let filtered: any[] = [];
        let query = event.query.toLowerCase();

        for (let i = 0; i < this.optionsDrugsUsage.length; i++) {
            let item = this.optionsDrugsUsage[i];
            if (
                item.name.toLowerCase().indexOf(query) !== -1 ||
                item.code.toLowerCase().indexOf(query) !== -1
            ) {
                filtered.push(item);
            }
        }
        this.filteredUsage = filtered;
    }

    selectedItemsClear() {
        this.selectedItems = '';
    }
    selectedUsageClear() {
        this.selectedUsage = '';
    }
    selectedQuantityClear() {
        this.selectedQuantity = '';
    }

    // save order oneday
    async saveOrderOneday() {
        await this.checkOrder();

        let orderData : any = {};
        if(this.is_success){
        // set order oneday
        if (this.selectedItems && this.selectedUsage  != '' && this.selectedQuantity != '') {
            orderData = {
                order_type_id: 1,
                item_type_id: this.selectedItems.item_type_id,
                item_id: this.selectedItems.id,
                item_name: this.selectedItems.name,
                medicine_usage_code: this.selectedUsage.code || null,
                medicine_usage_extra: this.selectedUsage.description || null,
                quantity: Number(this.selectedQuantity) || 1,
                is_confirm: false,
            };
        } else if (this.selectedItems && this.selectedUsage == '' && this.selectedQuantity != '') {
            orderData = {
                order_type_id: 1,
                item_type_id: this.selectedItems.item_type_id,
                item_id: this.selectedItems.id,
                item_name: this.selectedItems.name,
                quantity: Number(this.selectedQuantity) || 1,
                is_confirm: false,
            };
        } else if (this.selectedItems && this.selectedUsage != '' && this.selectedQuantity == '') {
            orderData = {
                order_type_id: 1,
                item_type_id: this.selectedItems.item_type_id,
                item_id: this.selectedItems.id,
                item_name: this.selectedItems.name,
                medicine_usage_code: this.selectedUsage.code || null,
                medicine_usage_extra: this.selectedUsage.description || null,
                quantity: 1,
                is_confirm: false,
            };
        } else if (this.selectedItems && this.selectedUsage == '' && this.selectedQuantity == '') {
            orderData = {
                order_type_id: 1,
                item_type_id: this.selectedItems.item_type_id,
                item_id: this.selectedItems.id,
                item_name: this.selectedItems.name,
                quantity: 1,
                is_confirm: false,
            };
        } else {
            alert('กรุณาเลือกข้อมูลให้ครบถ้วน');
            return;
        }

        try {

            let data = {
                doctor_order: this.doctorOrderData,
                orders: [orderData],
            };
            console.log('send data:', data);
            let res: any = await this.doctorOrderService.saveDoctorOrder(data);
            console.log('response:', res);
            let _order_id = res.data.data.order.id;

            // push to label
            this.orderOndayValue.push({
                id: _order_id,
                items: this.selectedItems,
                usage: this.selectedUsage || '',
                quantity: this.selectedQuantity || 1,
            });
            let parentData = { 'isNewOrder': false, 'doctor_order': this.doctorOrderData };

            sessionStorage.setItem(
                'doctor_order',
                JSON.stringify(parentData)
            );
        } catch (error) {
            console.log('save Doctor Order:' + error);
        }
        this.selectedItems = '';
        this.selectedUsage = '';
        this.selectedQuantity = '';
        } else {
            alert('ไม่สามารถบันทึกข้อมูลได้');
        }

    }

    getProgressNote() {
        console.log('progressNote');
        this.sidebarVisible2 = true;

        this.getList();
        //this.getProgresnote();
        this.getDx();
    }

    async getDx() {
        try {
            let response = await this.lookupService.getDx();
            let _dxData = response.data.data;
            this.dxData = _.sortBy(_dxData, [
                function (o) {
                    return o.name;
                },
            ]);
            console.log(this.dxData);
        } catch (error: any) {
            console.log('getLabItems' + `${error.code} - ${error.message}`);
        }
    }

    async getList() {
        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            const response = await this.doctorOrderService.getWaiting(
                _limit,
                _offset
            );

            const data: any = response.data;
            console.log(data);

            this.total = data.total || 1;

            this.dataSet = data.data.map((v: any) => {
                const date = v.admit_date
                    ? DateTime.fromISO(v.admit_date)
                          .setLocale('th')
                          .toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS)
                    : '';
                v.admit_date = date;
                return v;
            });
            console.log(this.dataSet);
        } catch (error: any) {
            console.log(`${error.code} - ${error.message}`);
        }
    }

    // get lookup medicine usage
    async getLookupMedicineUsage() {
        try {
            let response = await this.doctorOrderService.getLookupMedicine();
            this.optionsDrugs = response.data.data;

            // console.log(this.optionsDrugs);
        } catch (error: any) {
            console.log(error);
        }
    }
    optionsLabs: OptionItems[] = [];

    // check is new order
    async checkOrder() {
        console.log('checkOrder');
        let _data = sessionStorage.getItem('doctor_order');

        if (_data != 'undefined' && _data != null) {
            let _doctorOrderData = JSON.parse(_data);           
            this.is_new_order = _doctorOrderData.isNewOrder;
            this.doctorOrderData = _doctorOrderData.doctor_order;
        } else {
            this.is_new_order = true;
        }
        console.log('is_new_order',this.is_new_order);
        if(this.is_new_order){
            console.log('create new order');
            var date2 = DateTime.now();
            var date3 = DateTime.now();

            const date5 = date2.toFormat('yyyy-MM-dd');
            const time1 = date3.toFormat('HH:mm:ss');
    
            let data = {
                doctor_order: [{
                    admit_id: this.queryParamsData,
                    doctor_order_date: date5,
                    doctor_order_time: time1,
                    doctor_order_by: this.userData.id || null,
                    is_confirm: false,
                }]
            };
            console.log(data);
            try {
                let rs = await this.doctorOrderService.saveDoctorOrder(data);
                // console.log('response:',rs);
                this.doctorOrderData = rs.data.data.doctor_order;
                this.is_new_order = false;
                // sessionStorage.setItem('doctor_order', JSON.stringify(this.doctorOrderData));
                console.log('doctorOrderData:',this.doctorOrderData);
                this.is_success = true;
                let parentData = { 'isNewOrder': false, 'doctor_order': this.doctorOrderData };
                sessionStorage.setItem(
                    'doctor_order',
                    JSON.stringify(parentData)
                );
            } catch (error) {
                console.log('save Doctor Order:' + error);
                this.is_success = false;
            }

        } else {
            console.log('Use old order');

            this.is_success = true;
        }
    }

    async removeOrder(id: any) {
        try {
            let rs = await this.doctorOrderService.removeOrders(id);
            console.log('response:', rs);
            // this.getDoctorOrderById();

            // remove from label
            this.orderOndayValue = this.orderOndayValue.filter(
                (item:any) => item.id !== id
            );
        } catch (error) {
            console.log('remove Doctor Order:' + error);
        }
    }
}
