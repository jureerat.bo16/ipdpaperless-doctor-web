import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DoctorOrderService {

  pathPrefixLookup: any = `api-lookup/lookup`;
  pathPrefixDoctor: any = `api-doctor/doctor`;
  pathPrefixAuth: any = `api-auth/auth`;


  private axiosInstanceAuth = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixAuth}`
  });


  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixDoctor}`
  })

  private axiosLookup = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
  })

  constructor() {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })

    this.axiosLookup.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosLookup.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })
  }

  async getActive(wardId: any, query: any, limit: any, offset: any) {
    const url = `/admit?ward_id=${wardId}&query=${query}&limit=${limit}&offset=${offset}`
    return await this.axiosInstance.get(url)
  }

  async getWaiting(limit: any, offset: any) {
    const url = `/ward?limit=${limit}&offset=${offset}`
    return await this.axiosInstance.get(url)
  }

  async getPatientAdmit(wardId: any) {
    //  - /doctor/ward/:ward_id/patient-admit   #GET  
    // /doctor/admit/${wardID}/patient-admit
    const url = `/admit/${wardId}/patient-ward`
    return await this.axiosInstance.get(url)
  }


  async getPatientInfo(admitId: any) {
    // console.log(admitId);

    const url = `/admit/${admitId}/patient-info`
    // console.log(url);

    return await this.axiosInstance.get(url)
  }

  async saveAdmissionNote(data: any) {
    const url = `/admission-note`

    return await this.axiosInstance.post(url)
  }


  async saveDoctorOrder(data: object) {
    // console.log( sessionStorage.getItem('token'));
    return await this.axiosInstance.post('/doctor-order', data)
  }
  async updateDoctorOrder(data: object) {
    return await this.axiosInstance.put('/doctor-order', data)
  }

  async getDoctorOrderById(admitId: any) {
    console.log(admitId);

    const url = `/doctor-order/${admitId}/admit`

    return await this.axiosInstance.get(url)
  }

  async getDoctorOrderByDoctorOrderId(doctorOrderId: any) {
    const url = `/${doctorOrderId}/doctor-order`
    return await this.axiosInstance.get(url)
  }

  async removeOrders(OrderId: any) {
    const url = `/doctor-order/order/${OrderId}`
    return await this.axiosInstance.delete(url)
  }

  async getLookupMedicine() {
    // console.log('getLookupMedicine');
    const url = `/medicine`
    return await this.axiosLookup.get(url)
  }

  async getLookupItem() {
    const url = `/items/list`
    return await this.axiosLookup.get(url)
  }

  async saveProgressNote(data: object) {
    const url = `/progress-note`
    return await this.axiosInstance.post(url, data)
  }

  async getMedusage() {
    const url = `/medicine_usage`
    return await this.axiosLookup.get(url)
  }

  async getItems(itemTypeID: any) {
    // console.log('getItems');
    // /lookup/item/item-type/:itemTypeID
    const url = `/item/item-type/${itemTypeID}`
    return await this.axiosLookup.get(url)
  }
  async getStanding_progressnote(dxId: any) {
    //const url = `/standing-progress-note/infoGroupDiseaseID/${dxId}`;
    const url = `/standing-progress-note/infoGroupDiseaseID/${dxId}`;
   
    console.log(url);
    
    return await this.axiosLookup.get(url);
  }
  async getDxStardingorder() {
    const url = `/standing-order/getGroupDiseaseByStandingOrder`
    return await this.axiosInstance.get(url);
  }

  async getlockupstandinginfoGroupDiseaseID(itemTypeID: any) {
    // console.log('getItems');
    // /lookup/item/item-type/:itemTypeID
    const url = `/standing-order/infoGroupDiseaseID//${itemTypeID}`
    return await this.axiosLookup.get(url)
  }
  // http://209.15.114.75/api-nurse/nurse/his-services/waiting-lab?an=66000124&code=001
  async getUserInfo(id:any) {
    const url = `/user/${id}`;
    return this.axiosLookup.get(url);
  }

}
