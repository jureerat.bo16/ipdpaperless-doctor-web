import { Component } from '@angular/core';
import { Message, MessageService } from 'primeng/api';

import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { JwtHelperService } from '@auth0/angular-jwt';

import { FormBuilder,FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { UserProfileApiService } from './user-profile.service';

@Component({
    templateUrl: './login.component.html',
    providers: [MessageService],
})
export class LoginComponent {
    jwtHelper: JwtHelperService = new JwtHelperService();

    public validateForm!: FormGroup;
    public passwordVisible: Boolean = false;
    messages: any | undefined;
    loading:boolean=false;
    formGroup!: FormGroup;
    constructor(
        private layoutService: LayoutService,
        public login: FormBuilder,
        private loginService: LoginService,
        private router: Router,
        private messageService: MessageService,
        private userProfileApiService: UserProfileApiService
    ) {}

    get filledInput(): boolean {
        return this.layoutService.config.inputStyle === 'filled';
    }

    ngOnInit(): void {
        this.formGroup = new FormGroup({
            value: new FormControl()
        });
        this.validateForm = this.login.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required]],
        });
    }
  

    async onSubmit() {
        this.loading = true;
        console.log('submit');

        for (const i in this.validateForm.controls) {
            this.validateForm.controls[i].markAsDirty();
            this.validateForm.controls[i].updateValueAndValidity();
        }

        if (this.validateForm.status == 'INVALID') {
            console.log('INVALID');
            this.messageService.add({
                severity: 'error',
                summary: 'เกิดข้อผิดพลาด !',
                detail: 'กรูณาตรวจสอบ',
            });

            this.loading = false;

            return;
        }

        if (this.validateForm.status == 'VALID') {
            let { username, password } = this.validateForm.value;
            console.log(username);
            console.log(password);

            try {
                const response: any = await this.loginService.login(
                    username,
                    password
                );

                if (response.data) {
                    this.loading = false;

                    const token = response.data.accessToken;
                    sessionStorage.setItem('token', token);

                    this.messageService.add({
                        severity: 'success',
                        summary: 'ล๊อกอินสำเร็จ #',
                        detail: 'รอสักครู่...',
                        icon:'<i class="pi pi-spin pi-spinner cursor-pointer text-sm" style="line-height: 1.25;"></i>'
                    });

                    const decoded = this.jwtHelper.decodeToken(token);
                    await this.getUserById();
                } else {
                    this.loading = false;
                }
            } catch (error: any) {
                console.log(error);
                this.messageService.add({
                    severity: 'error',
                    summary: 'ไม่สามารถล๊อกอินได้ !',
                    detail: '',
                });
                this.loading = false;

                // this.router.navigate(['/doctor/listward']);
            }
        }
    }

    async getUserById() {
        console.log('getUserById');
        try {
            let data = await this.userProfileApiService.getUserInfo();
            console.log(data);
            let dk =
                data.data[0].title +
                '' +
                data.data[0].fname +
                ' ' +
                data.data[0].lname;
            sessionStorage.setItem('userLoginName', dk);
            sessionStorage.setItem('userData', JSON.stringify(data.data[0]));
            setTimeout(() => {
                this.loading = false;
				this.router.navigate(['/']);
            }, 5000);

            
        } catch (error) {
            console.log(error);
        }
    }
    logings() {
        this.router.navigate(['/doctor']);
    }
}
