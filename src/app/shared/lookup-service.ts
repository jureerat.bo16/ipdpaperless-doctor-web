import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LookupService {

  pathPrefixLookup: any = `api-lookup/lookup`
  pathPrefixDoctor: any = `api-doctor/doctor`
  pathPrefixAuth: any = `api-auth/auth`

  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixLookup}`
  });

  private axiosInstancedtr = axios.create({
    baseURL: `${environment.apiUrl}${this.pathPrefixDoctor}`
  });

  constructor () {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token');
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
    });

    this.axiosInstancedtr.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token');
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }
      return config;
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response;
    }, error => {
      return Promise.reject(error);
    });

    this.axiosInstancedtr.interceptors.response.use(response => {
      return response;
    }, error => {
      return Promise.reject(error);
    })
  }

  async getWard() {
    const url = `/ward`;
    return this.axiosInstance.get(url);
  }

  async getProgressNote() {
    const url = `/standing-progress-note`
    return await this.axiosInstance.get(url);
  }
  async getStanding(dxId: any) {
    //const url = `/standing-progress-note/infoGroupDiseaseID/${dxId}`;
    const url = `/standing-progress-note/infoGroupDiseaseID/${dxId}`;
   
    console.log(url);
    
    return await this.axiosInstance.get(url);
  }

  async getLastSendings(start: any, end: any) {
    const url = `/last-sending?start=${start}&end=${end}`;
    return this.axiosInstance.get(url);
  }

  async getDx() {
    const url = `/group-disease`
    return await this.axiosInstance.get(url);
  }
  async getDxStardingorder() {
    const url = `/standing-order/getGroupDiseaseByStandingOrder`
    return await this.axiosInstancedtr.get(url);
  }
}
